@extends('admin.layout.index')

@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tác giả / Author
                            <small>Danh sách</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if (session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Tên tác giả</th>
                                <th>Tên không dấu</th>
                                <th>Hình ảnh</th>
                                <th style="text-align: center;">Mô tả</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($author as $key)
                            <tr class="odd gradeX" align="center">
                                <td>{{$key->id}}</td>
                                <td>{{$key->ten}}</td>
                                <td>{{$key->tenkhongdau}}</td>
                                <td><p> <img src="source/upload/author/{{$key->hinh}}" width="100px" height="100px"> </p></td>
                                <td>{{$key->mota}}</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/author/xoa/{{$key->id}}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/author/sua/{{$key->id}}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection
