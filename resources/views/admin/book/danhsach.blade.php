@extends('admin.layout.index')

@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sách / Book
                            <small>Danh sách</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if (session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Tên sách</th>
                                <th>Người đăng tải</th>
                                <th>Thể loại sách</th>
                                <th>Tác giả</th>
                                <th>Nhà sản xuất</th>
                                <th>Số trang</th>
                                <th>Giá tiền</th>
{{--                                <th >Mô tả</th>--}}
                                <th>Hình</th>
                                <th>Số lượng</th>
                                <th>Trạng thái</th>
                                <th>Phí giao hàng</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($book as $key)
                            <tr class="odd gradeX" align="center">
                                <td>{{$key->id}}</td>
                                <td>{{$key->ten}}</td>
                                <td>{{$key->user->name}}</td>
                                <td>{{$key->typebook->ten}}</td>
                                <td>{{$key->author->ten}}</td>
                                <td>{{$key->publisher->ten}}</td>
                                <td>{{$key->sotrang}}</td>
                                <td>{{$key->giatien}}</td>
{{--                                <td>{{$key->mota}}</td>--}}
                                <td><p><img src="source/upload/book/{{$key->hinh}}" width="100px" height="100px"></p></td>
                                <td>{{$key->soluottruycap}}</td>
                                <td>
                                    @if ($key->trangthai == 0)
                                        {{'Cũ'}}
                                    @else
                                        {{'Mới'}}
                                    @endif
                                </td>
                                <td>{{$key->phigiaohang}}</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/book/xoa/{{$key->id}}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/book/sua/{{$key->id}}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection
