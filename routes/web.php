<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\AuthorController;
use App\Http\Controllers\PageController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

//=========================USER====================================

Route::get('/gioithieu', 'PageController@gioiThieu');
Route::get('/lienhe', 'PageController@lienHe');
//show
Route::get('/trangchu', 'PageController@trangChu')->name('trangchu');
Route::get('loaisanpham/{id}', 'PageController@loaiSanPham');
Route::get('tacgia/{id}', 'PageController@tacgia');
Route::get('nhasanxuat/{id}', 'PageController@nhasanxuat');
Route::get('/chitietsanpham/{id}', 'PageController@chitietsp')->name('chitietsp');
//seach
Route::post('/timkiem', 'PageController@timKiem')->name('timkiem');
//dn/dk/dx
Route::get('/dangnhap', 'PageController@getDangnhap')->name('dangnhap');
Route::post('/dangnhap', 'PageController@postDangnhap');
Route::get('/dangxuat', 'PageController@dangXuat');
Route::get('/dangky', 'PageController@getDangKy')->name('dangky');
Route::post('/dangky', 'PageController@postDangKy');

//nguoi dung cap 2
//thong tin ca nhan
Route::get('suathongtincanhan', 'PageController@getThongtincanhan')->name('suathongtincanhan');
Route::post('suathongtincanhan', 'PageController@postThongtincanhan');

Route::post('feedback/{bookId}', 'PageController@postFeedback');

Route::group(['prefix' => 'upload'], function () {
    Route::get('them', 'PageController@getThemBook');
    Route::post('them', 'PageController@postThemBook');
});

Route::get('themgiohang/{idBook}/{num}', 'PageController@themGioHang');
Route::get('xoagiohang/{id}', 'PageController@xoaGioHang');
Route::get('dathang', 'PageController@getDatHang');
Route::post('dathang', 'PageController@postDatHang');

//=========================ADMIN===================================
Route::get('admin/dangnhap', 'AdminController@getDangnhap')->name('admin_dangnhap');
Route::post('admin/dangnhap', 'AdminController@postDangnhap');
Route::get('admin/logout', 'AdminController@dangxuat');

Route::group(['prefix' => 'admin', 'middleware' => 'adminlogin'], function () {

    Route::group(['prefix' => 'typebook'], function () {
        Route::get('danhsach', 'TypeBookController@getTypebook')->name('danhsachTypebook');
        Route::get('xoa/{idtypebook}', 'TypeBookController@delTypebook');
        Route::get('sua/{editTypebook}','TypeBookController@getEditTypebook');
        Route::post('sua/{editTypebook}','TypeBookController@postEditTypebook');
        Route::get('them','TypeBookController@getCreateTypebook');
        Route::post('them','TypeBookController@postCreateTypebook');
    });

    Route::group(['prefix' => 'author'], function () {
        Route::get('them', 'AuthorController@getCreateAuthor');
        Route::post('them', 'AuthorController@postCreateAuthor');
        Route::get('danhsach', 'AuthorController@danhsachAuthor');
        Route::get('xoa/{id}', 'AuthorController@xoaAuthor');
        Route::get('sua/{id}', 'AuthorController@getEditAuthor');
        Route::post('sua/{id}', 'AuthorController@postEditAuthor');

    });

    Route::group(['prefix'=>'publisher'], function () {
        Route::get('them', 'PublicerController@getAddPublicer');
        Route::post('them', 'PublicerController@postAddPublicer');
        Route::get('danhsach', 'PublicerController@listPublicer');
        Route::get('xoa/{id}', 'PublicerController@xoaPublicer');
        Route::get('sua/{id}', 'PublicerController@getEditPublicer');
        Route::post('sua/{id}', 'PublicerController@postEditPublicer');
    });

    Route::group(['prefix'=>'book'], function () {
        Route::get('them', 'BookController@getAddBook');
        Route::post('them', 'BookController@postAddBook');
        Route::get('danhsach', 'BookController@listBook');
        Route::get('xoa/{id}', 'BookController@xoaBook');
        Route::get('sua/{id}', 'BookController@getEditBook');
        Route::post('sua/{id}', 'BookController@postEditBook');
    });

    Route::group(['prefix'=>'customer'], function () {
        Route::get('them', 'CustomerController@getAddCustomer');
        Route::post('them', 'CustomerController@postAddCustomer');
        Route::get('danhsach', 'CustomerController@listCustomer');
        Route::get('xoa/{id}', 'CustomerController@xoaCustomer');
        Route::get('sua/{id}', 'CustomerController@getEditCustomer');
        Route::post('sua/{id}', 'CustomerController@postEditCustomer');
    });

    Route::group(['prefix'=>'slide'], function () {
        Route::get('them', 'SlideController@getAddSlide');
        Route::post('them', 'SlideController@postAddSlide');
        Route::get('danhsach', 'SlideController@listSlide');
        Route::get('xoa/{id}', 'SlideController@xoaSlide');
        Route::get('sua/{id}', 'SlideController@getEditSlide');
        Route::post('sua/{id}', 'SlideController@postEditSlide');
    });

    Route::group(['prefix'=>'user'], function () {
        Route::get('them', 'UserController@getAddUser');
        Route::post('them', 'UserController@postAddUser');
        Route::get('danhsach', 'UserController@listUser');
        Route::get('xoa/{id}', 'UserController@xoaUser');
        Route::get('sua/{id}', 'UserController@getEditUser');
        Route::post('sua/{id}', 'UserController@postEditUser');
    });

    Route::group(['prefix'=>'bills'], function () {
        Route::get('them', 'BillController@getAddBill');
        Route::post('them', 'BillController@postAddBill');
        Route::get('danhsach', 'BillController@listBill');
        Route::get('xoa/{id}', 'BillController@xoaBill');
        Route::get('sua/{id}', 'BillController@getEditBill');
        Route::post('sua/{id}', 'BillController@postEditBill');
    });

    Route::group(['prefix'=>'detailbill'], function () {
        Route::get('them', 'BilldetailController@getAddBilldetail');
        Route::post('them', 'BilldetailController@postAddBilldetail');
        Route::get('danhsach', 'BilldetailController@listDetailBill');
        Route::get('xoa/{id}', 'BilldetailController@xoaBilldetail');
        Route::get('sua/{id}', 'BilldetailController@getEditBilldetail');
        Route::post('sua/{id}', 'BilldetailController@postEditBilldetail');
    });

});





