<?php

namespace App\Http\Controllers;

use App\Book;
use App\TypeBook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TypeBookController extends Controller
{
//R
    public function getTypebook(){
        $typebook = TypeBook::all();
        return view('admin.typebook.danhsach')->with('typebook',$typebook);
    }
//D
    public function delTypebook($idtypebook){
        //xoa typebooks vs books->type
        $bookType = TypeBook::find($idtypebook);
        $bookType->delete();
        return redirect()->route('danhsachTypebook')->with('thongbao', 'Xóa thành công');
    }
//U
    public function getEditTypebook($editTypebook){
        $typeBook = TypeBook::find($editTypebook);
        return view('admin.typebook.sua')->with('typebook',$typeBook);
    }

    public function postEditTypebook(Request $request, $editTypebook){
        $this->validate($request,
            [
                'ten'=>'required|min:3',
                'mota'=>'required'
            ],
            [
                'ten.required'=>'Bạn chưa nhập tên thể loại sách',
                'ten.min'=>'Tên thể loại sách quá ngắn',
                'mota.required'=>'Mô tả rỗng, cần mô tả',
            ]);

        $typeBook = TypeBook::find($editTypebook);
        $typeBook->ten = $request->ten;
        $typeBook->mota = $request->mota;
        $typeBook->save();
        return redirect()->route('danhsachTypebook')->with('thongbao','Sửa thành công');
    }

    public function getCreateTypebook(){
        return view('admin.typebook.them');
    }

    public function postCreateTypebook(Request $request){
        $this->validate($request, [
            'ten' => 'required|unique:type_book,ten|min:3',
            'mota' => 'required'
        ], [
            'ten.required' => 'Bạn chưa nhập tên loại sách',
            'ten.unique' => 'Tên sách đã tồn tại',
            'ten.min' => 'Tên loại sách ít nhất 3 ký tự',
            'mota' => 'Bạn chưa nhập mô tả'
        ]);

        $typeBook = new TypeBook();
        $typeBook->ten=$request->ten;
        $typeBook->tenkhongdau=changeTitle($request->ten);
        $typeBook->mota=$request->mota;
        $typeBook->save();

        return redirect()->route('danhsachTypebook')->with('thongbao', 'Thêm loại sách thành công');

    }

}
