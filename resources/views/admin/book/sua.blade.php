@extends('admin.layout.index')

@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sách / Book
                            <small>Sửa</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if (count($errors)>0)
                            <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                            </div>
                        @endif
                        @if (session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                        <form action="admin/book/sua/{{$book->id}}" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label>Tên sách</label>
                                <input class="form-control" name="ten" placeholder="Nhập tên sách" value="{{$book->ten}}" />
                            </div>
                            <div class="form-group">
                                <label>Mô tả</label>
                                <textarea name="mota" id="mota" class="form-control ckeditor" rows="4">{{$book->mota}}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <img src="source/upload/book/{{$book->hinh}}" width="100px" height="100px">
                                <input class="form-control" id="hinh" type="file" name="hinh">
                            </div>
                            <div class="form-group">
                                <label>Người đăng tải</label>
                                <select class="form-control" name="idUser">
                                    @foreach ($user as $key)
                                    <option
                                    @if ($key->id == $book->idUser)
                                    {{'selected'}}
                                    @endif
                                    value="{{$key->id}}">{{$key->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Thể loại</label>
                                <select class="form-control" name="idLoaisach">
                                    @foreach ($typebook as $key)
                                    <option
                                    @if ($key->id == $book->idTheloai)
                                    {{'selected'}}
                                    @endif
                                    value="{{$key->id}}">{{$key->ten}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tác giả</label>
                                <select class="form-control" name="idTacgia">
                                    @foreach ($author as $key)
                                    <option
                                    @if ($key->id == $book->idTacgia)
                                    {{'selected'}}
                                    @endif
                                    value="{{$key->id}}">{{$key->ten}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nhà sản xuất</label>
                                <select class="form-control" name="idNhasanxuat">
                                    @foreach ($publisher as $key)
                                    <option
                                    @if ($key->id == $book->idNhasanxuat)
                                    {{'selected'}}
                                    @endif
                                    value="{{$key->id}}">{{$key->ten}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Số trang</label>
                                <input class="form-control" name="sotrang" placeholder="Nhập số trang" value="{{$book->sotrang}}" />
                            </div>
                            <div class="form-group">
                                <label>Giá tiền (VNĐ)</label>
                                <input class="form-control" name="giatien" placeholder="Nhập giá tiền" value="{{$book->giatien}}" />
                            </div>
                            <div class="form-group">
                                <label>Số lượng</label>
                                <input class="form-control" name="soluottruycap" placeholder="Nhập số lượng" value="{{$book->soluottruycap}}" />
                            </div>
                            <div class="form-group">
                                <label>Phí giao hàng (VNĐ)</label>
                                <input class="form-control" name="phigiaohang" placeholder="Nhập phí giao hàng" value="{{$book->phigiaohang}}" />
                            </div>
                            <div class="form-group">
                                <label>Trạng thái</label>
                                <label class="radio-inline">
                                    <input name="trangthai" value="1"
                                    @if ($book->trangthai == 1)
                                    {{'checked'}}
                                    @endif
                                    type="radio">Mới
                                </label>
                                <label class="radio-inline">
                                    <input name="trangthai" value="0"
                                    @if ($book->trangthai == 0)
                                    {{'checked'}}
                                    @endif
                                    type="radio">Cũ
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default">Sửa</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Phản hồi / Feedback
                            <small>Danh sách</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if (session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Người phản hồi</th>
                                <th>Nội dung</th>
                                <th>Ngày đăng</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($book->feedback as $key)
                            <tr class="odd gradeX" align="center">
                                <td>{{$key->id}}</td>
                                <td>{{$key->user->name}}</td>
                                <td>{{$key->noidung}}</td>
                                <td>{{$key->created_at}}</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/feedback/xoa/{{$key->id}}/{{$book->id}}">Xóa</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row commet-->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection
