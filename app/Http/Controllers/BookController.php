<?php

namespace App\Http\Controllers;

use App\Author;
use App\Book;
use App\Publisher;
use App\TypeBook;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class BookController extends Controller
{
    //
    public function getAddBook(){
        $author = Author::all();
        $publisher = Publisher::all();
        $typebook = TypeBook::all();
        $user = User::where('quyen', 1)->take(5)->get();

        return view('admin.book.them')->with(['author'=>$author,'publisher'=>$publisher,'typebook'=>$typebook,'user'=>$user]);
    }

    public function postAddBook(Request $request){
        $this->validate($request,
            [
                'ten'=>'required',
                'idUser'=>'required',
                'idLoaisach'=>'required',
                'idTacgia'=>'required',
                'idNhasanxuat'=>'required',
                'sotrang'=>'required|integer',
                'giatien'=>'required|integer',
                'soluottruycap'=>'required|integer',
                'phigiaohang'=>'required|integer',
            ],
            [
                'ten.required'=>'Bạn chưa nhập tên sách',
                'idUser.required'=>'Bạn cần chọn người đăng tải',
                'idLoaisach.required'=>'Bạn cần chọn thể loại sách',
                'idTacgia.required'=>'Bạn cần chọn tác giả sách',
                'idNhasanxuat.required'=>'Bạn cần chọn nhà xuất bản',
                'sotrang.required'=>'Bạn chưa nhập số trang',
                'giatien.required'=>'Bạn chưa nhập giá sách',
                'soluottruycap.required'=>'Bạn chưa nhập số lượng',
                'phigiaohang.required'=>'Bạn chưa nhập phí giao hàng',
            ]);

        $book = new Book();
        $book->ten =$request->ten;
        $book->tenkhongdau =changeTitle($request->ten);
        $book->mota = $request->mota;
        $book->sotrang = $request->sotrang;
        $book->giatien = $request->giatien;
        $book->soluottruycap = $request->soluottruycap;
        $book->trangthai = $request->trangthai;
        $book->phigiaohang = $request->phigiaohang;
        $book->idUser = $request->idUser;
        $book->idLoaisach = $request->idLoaisach;
        $book->idTacgia = $request->idTacgia;
        $book->idNhasanxuat = $request->idNhasanxuat;


        if ($request->hasFile('hinh')) {
            $file = $request->file('hinh');
            $newFile = time() . '.' . $file->getClientOriginalExtension();
            $file->move(Config::get('MyConst.book_img_folder'), $newFile);
            $book->hinh = $newFile;
        }else{
            $book->hinh = "";
        }
        $book->save();

        return redirect()->back()->with('thongbao', 'Thêm sách thành công');
    }

    //
    public function listBook(){
        $books  = Book::all();
        return view('admin.book.danhsach')->with('book',$books )
            ;
    }

    //
    public function getEditBook($id){
        $book  = Book::find($id);
        $author = Author::all();
        $publisher = Publisher::all();
        $typebook = TypeBook::all();
        $user = User::where('quyen', 1)->take(5)->get();

        return view('admin.book.sua')->with('book',$book)
            ->with(['author'=>$author,'publisher'=>$publisher,'typebook'=>$typebook,'user'=>$user])
            ;
    }

    public function postEditBook(Request $request,$id){
        $this->validate($request,
            [
                'ten'=>'required',
                'idUser'=>'required',
                'idLoaisach'=>'required',
                'idTacgia'=>'required',
                'idNhasanxuat'=>'required',
                'sotrang'=>'required|integer',
                'giatien'=>'required|integer',
                'soluottruycap'=>'required|integer',
                'phigiaohang'=>'required|integer',
            ],
            [
                'ten.required'=>'Bạn chưa nhập tên sách',
                'idUser.required'=>'Bạn cần chọn người đăng tải',
                'idLoaisach.required'=>'Bạn cần chọn thể loại sách',
                'idTacgia.required'=>'Bạn cần chọn tác giả sách',
                'idNhasanxuat.required'=>'Bạn cần chọn nhà xuất bản',
                'sotrang.required'=>'Bạn chưa nhập số trang',
                'giatien.required'=>'Bạn chưa nhập giá sách',
                'soluottruycap.required'=>'Bạn chưa nhập số lượng',
                'phigiaohang.required'=>'Bạn chưa nhập phí giao hàng',
            ]);

        $book =Book::find($id);

        $book->ten =$request->ten;
        $book->tenkhongdau =changeTitle($request->ten);
        $book->mota = $request->mota;
        $book->sotrang = $request->sotrang;
        $book->giatien = $request->giatien;
        $book->soluottruycap = $request->soluottruycap;
        $book->trangthai = $request->trangthai;
        $book->phigiaohang = $request->phigiaohang;
        $book->idUser = $request->idUser;
        $book->idLoaisach = $request->idLoaisach;
        $book->idTacgia = $request->idTacgia;
        $book->idNhasanxuat = $request->idNhasanxuat;


        if ($request->hasFile('hinh')) {
            $file = $request->file('hinh');
            $newFile = time() . '.' . $file->getClientOriginalExtension();
            $file->move(Config::get('MyConst.book_img_folder'), $newFile);
            $book->hinh = $newFile;
        }
        $book->save();

        return redirect()->back()->with('thongbao', 'Sửa thành công');
    }

    //
    public function xoaBook($id){
        $publisher = Book::find($id);
        if ($publisher) {
            $publisher->delete();
            return redirect()->back()->with('thongbao',"Xóa thông tin sách thành công");
        }
        return redirect()->back()->with('thongbao',"Sách không tồn tại");

    }
}
