<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    //
    protected $table="Book";

    public function feedback(){
        return $this->hasMany('App\Feedback', 'idBook', 'id');

    }
    public function author(){
        return $this->belongsTo('App\Author', 'idTacgia', 'id');
    }
    public function typebook(){
        return $this->belongsTo('App\TypeBook', 'idLoaisach', 'id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'idUser', 'id');
    }

    public function publisher(){
        return $this->belongsTo('App\Publisher', 'idNhasanxuat', 'id');
    }

}
