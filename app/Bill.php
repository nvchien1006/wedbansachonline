<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    //
    public $table = "bills";

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'idCustomer', 'id');
    
    }

}
