<?php

namespace App\Http\Controllers;

use App\Bill;
use App\Customer;
use App\DetailBill;
use Illuminate\Http\Request;

class BillController extends Controller
{
    //
    public function listBill()
    {
        $bill = Bill::all();
        return view('admin.bills.danhsach', ['bills' => $bill]);
    }

    public function getAddBill()
    {
        $customer = Customer::all();
        return view('admin.bills.them')->with('customer', $customer);
    }


    public function postAddBill(Request $request)
    {
        $this->validate($request,
            [
                'idCustomer'=>'required',
                'ngaydonhang'=>'required',
                'tongtien'=>'required|integer',
            ],
            [
                'idcustomer.required'=>'Bạn chưa nhập khách hàng của hóa đơn',
                'ngaydonhang.required'=>'Bạn cần nhập ngày đơn hàng',
                'tongtien.required'=>'Bạn cần nhập tổng tiền của hóa đơn',
            ]);

        if ($request->tongtien > 0) {
            $bill = new Bill;
            $bill->idCustomer = $request->idCustomer;
            $bill->ngaydonhang = $request->ngaydonhang;
            $bill->tongtien = $request->tongtien;
            $bill->trangthai = $request->trangthai;
            $bill->notes = $request->notes;
            $bill->save();

            return redirect('admin/bills/them')->with('thongbao', 'Thêm thành công');
        }
        else{
                return redirect('admin/bills/them')->with('thongbao', 'Tổng tiền phải lớn hơn 0');
        }
    }


    public function getEditBill($id)
    {
        $customer = Customer::all();
        $bill = Bill::find($id);
        return view('admin.bills.sua', ['customer' => $customer, 'bill' => $bill]);
    }


    public function postEditBill(Request $request, $id){
        $this->validate($request,
            [
                'idCustomer'=>'required',
                'ngaydonhang'=>'required',
                'tongtien'=>'required|integer',
            ],
            [
                'idcustomer.required'=>'Bạn chưa nhập khách hàng của hóa đơn',
                'ngaydonhang.required'=>'Bạn cần nhập ngày đơn hàng',
                'tongtien.required'=>'Bạn cần nhập tổng tiền của hóa đơn',
            ]);


        if ($request->tongtien > 0) {
            $bill=Bill::find($id);
            $bill->idCustomer = $request->idCustomer;
            $bill->ngaydonhang = $request->ngaydonhang;
            $bill->tongtien = $request->tongtien;
            $bill->trangthai = $request->trangthai;
            $bill->notes = $request->notes;
            $bill->save();

            return redirect()->back()->with('thongbao', 'Thêm thành công');
        }
        else{
            return redirect()->back()->with('thongbao', 'Tổng tiền phải lớn hơn 0');
        }
    }

    public function xoaBill($id)
    {
        $bill = Bill::find($id);
        if ($bill != null) {
            $bill->delete();
            return redirect()->back()->with('thongbao', 'Xóa thành công');
        }
    }
}
