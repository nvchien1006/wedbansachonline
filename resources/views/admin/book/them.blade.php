    @extends('admin.layout.index')

    @section('content')
    <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Sách / Book
                                <small>Thêm</small>
                            </h1>
                        </div>
                        <!-- /.col-lg-12 -->
                        <div class="col-lg-7" style="padding-bottom:120px">
                            @if (count($errors)>0)    
                                <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                    {{$err}}<br>
                                @endforeach
                                </div>
                            @endif
                            @if (session('thongbao'))
                                <div class="alert alert-success">
                                    {{session('thongbao')}}
                                </div>
                            @endif
                            <form action="admin/book/them" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="form-group">
                                    <label>Tên sách</label>
                                    <input class="form-control" name="ten" placeholder="Nhập tên sách" />
                                </div>
                                <div class="form-group">
                                    <label>Mô tả</label>
                                    <textarea name="mota" id="mota" class="form-control ckeditor" rows="4"></textarea>
                                </div> 
                                <div class="form-group">
                                    <label>Hình ảnh</label>                                    
                                    <input class="form-control" type="file" name="hinh">
                                </div>
                                <div class="form-group">
                                    <label>Người đăng tải</label>
                                    <select class="form-control" name="idUser">
                                        @foreach ($user as $key)
                                        <option value="{{$key->id}}">{{$key->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Thể loại</label>
                                    <select class="form-control" name="idLoaisach">
                                        @foreach ($typebook as $key)
                                        <option value="{{$key->id}}">{{$key->ten}}</option> 
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Tác giả</label>
                                    <select class="form-control" name="idTacgia">
                                        @foreach ($author as $key)
                                        <option value="{{$key->id}}">{{$key->ten}}</option>                                    
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Nhà sản xuất</label>
                                    <select class="form-control" name="idNhasanxuat">
                                        @foreach ($publisher as $key)
                                        <option value="{{$key->id}}">{{$key->ten}}</option>                                    
                                        @endforeach
                                    </select>
                                </div>                            
                                <div class="form-group">
                                    <label>Số trang</label>
                                    <input class="form-control" name="sotrang" placeholder="Nhập số trang" />
                                </div>                            
                                <div class="form-group">
                                    <label>Giá tiền (VNĐ)</label>
                                    <input class="form-control" name="giatien" placeholder="Nhập giá tiền" />
                                </div>
                                <div class="form-group">
                                    <label>Số lượng</label>
                                    <input class="form-control" name="soluottruycap" placeholder="Nhập số lượng" value="0" />
                                </div> 
                                <div class="form-group">
                                    <label>Phí giao hàng (VNĐ)</label>
                                    <input class="form-control" name="phigiaohang" placeholder="Nhập phí giao hàng" value="0" />
                                </div>                            
                                <div class="form-group">
                                    <label>Trạng thái</label>
                                    <label class="radio-inline">
                                        <input name="trangthai" value="1" checked="" type="radio">Mới
                                    </label>
                                    <label class="radio-inline">
                                        <input name="trangthai" value="0" type="radio">Cũ
                                    </label>
                                </div>                            
                                <button type="submit" class="btn btn-default">Thêm</button>
                                <button type="reset" class="btn btn-default">Làm mới</button>
                            <form>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
    @endsection