<?php

namespace App\Http\Controllers;

use App\Book;
use App\Publisher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class PublicerController extends Controller
{
    //
    public function getAddPublicer(){
        return view('admin.publicer.them');
    }

    public function postAddPublicer(Request $request){
        $this->validate($request,[
                'ten'=>'required',
                'mota'=>'required'
            ],
            [
                'ten.required'=>'Bạn chưa nhập tên nhà sản xuất',
                'mota.required'=>'Mô tả rỗng, cần mô tả',
            ]);
        $publisher = new Publisher();
        $publisher->ten =$request->ten;
        $publisher->tenkhongdau =changeTitle($request->ten);
        $publisher->mota = $request->mota;
        if ($request->hasFile('hinh')) {
            $file = $request->file('hinh');
            $newFile = time() . '.' . $file->getClientOriginalExtension();
            $file->move(Config::get('MyConst.publisher_img_folder'), $newFile);
            $publisher->hinh = $newFile;
        }else{
            $publisher->hinh = "";
        }
        $publisher->save();

        return redirect()->back()->with('thongbao', 'Thêm thành công');
    }

    //
    public function listPublicer(){
        $publisher  = Publisher::all();
        return view('admin.publicer.danhsach')->with('publisher',$publisher );
    }

    //
    public function getEditPublicer($id){
        $publisher  = Publisher::find($id);
        return view('admin.publicer.sua')->with('publisher',$publisher);
    }

    public function postEditPublicer(Request $request,$id){
        $this->validate($request,[
            'ten'=>'required',
            'mota'=>'required'
        ],
        [
            'ten.required'=>'Bạn chưa nhập tên nhà sản xuất',
            'mota.required'=>'Mô tả rỗng, cần mô tả',
        ]);
        $publisher = Publisher::find($id);
        $publisher->ten =$request->ten;
        $publisher->tenkhongdau =changeTitle($request->ten);
        $publisher->mota = $request->mota;
        if ($request->hasFile('hinh')) {
            $file = $request->file('hinh');
            $newFile = time() . '.' . $file->getClientOriginalExtension();
            $file->move(Config::get('MyConst.publisher_img_folder'), $newFile);
            $publisher->hinh = $newFile;
        }
        $publisher->save();

        return redirect()->back()->with('thongbao', 'Sửa thành công');
    }

    //
public function xoaPublicer($id){
    $publisher = Publisher::find($id);
    $publisher->delete();
    return redirect()->back()->with('thongbao',"Xóa thành công");
}

}
