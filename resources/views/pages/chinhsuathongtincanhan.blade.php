@extends('layout.index')
@section('content')
    <div class="inner-header">
        <div class="container">
            <div class="pull-left">
                <h6 class="inner-title"><a href="suathongtincanhan" style="font-size: 20px; color: blue;"> Chỉnh sửa
                        thông tin</a></h6>
            </div>

            <div class="pull-right">
                <div class="beta-breadcrumb">
                    <a href="trangchu">Home</a> / <span>Edit Information</span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="container">
        <div id="content">
            @if (count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        {{$err}}<br>
                    @endforeach
                </div>
            @endif
            @if (session('thongbao'))
                <div class="alert alert-success">
                    {{session('thongbao')}}
                </div>
            @endif
            <form action="suathongtincanhan" method="post" class="beta-form-checkout" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                        <h4>Chỉnh sửa thông tin cá nhân</h4>
                        <div class="space20">&nbsp;</div>
                        <div class="form-block">
                            <label for="ten">Họ tên *:</label>
                            <input type="text" id="ten" name="ten" value="{{$user->name}}">
                        </div>
                        <div class="form-block">
                            <label for="ten">Email *:</label>
                            <input type="text" id="email" name="email" value="{{$user->email}}">
                        </div>
                        <div class="form-block">
                            <label for="ten">Tên đăng nhập *:</label>
                            <input type="text" id="tendangnhap" name="tendangnhap" value="{{$user->tendangnhap}}" disabled="">
                        </div>
                        <div class="form-block">
                            <label for="your_last_name">Hình ảnh *:</label>
                            <div>
                                <img src="source/upload/user/{{$user->hinh}}" width="100px" height="100px">
                                <input type="file" id="hinh" name="hinh"></div>
                        </div>

                        <div class="form-block">
                            <label>Đổi mật khẩu</label>
                            <input type="password" class="form-control password" name="password"
                            placeholder="*****************"/>
                        </div>
                        <div class="form-block">
                            <label>Nhập lại mật khẩu</label>
                            <input type="password" class="form-control password" name="passwordAgain"
                                   placeholder="*****************"/>
                        </div>
                        <button type="submit" class="btn btn-default">Sửa</button>
                        <button type="reset" class="btn btn-default">Làm mới</button>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
            </form>
        </div> <!-- #content -->
    </div> <!-- .container -->
@endsection
@section('script')
@endsection
