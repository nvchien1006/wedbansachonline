@extends('admin.layout.index')
@section('content')
	<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Khách hàng /Customer
                            <small>Sửa</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                    	@if (count($errors)>0)    
                            <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                            </div>
                        @endif
                        @if (session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                        <form action="admin/customer/sua/{{$customer->id}}" method="POST">
                        	<input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label>Tên khách hàng</label>
                                <input class="form-control" name="ten" placeholder="Nhập tên khách hàng" value="{{$customer->ten}}" />
                            </div>
                            <div class="form-group">
                                <label>Giới tính</label>
                                <input class="form-control" name="gioitinh" placeholder="Điền giới tính" value="{{$customer->gioitinh}}" />
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" placeholder="Nhập E-mail" value="{{$customer->email}}" readonly="" />
                            </div>
                            <div class="form-group">
                                <label>Địa chỉ</label>
                                <input class="form-control" name="diachi" placeholder="Nhập địa chỉ giao hàng" value="{{$customer->diachi}}" />
                            </div>
                            <div class="form-group">
                                <label>Số điện thoại</label>
                                <input class="form-control" name="sodienthoai" placeholder="Nhập số điện thoại" value="{{$customer->sodienthoai}}" />
                            </div>
                            <div class="form-group">
                                <label>Chú ý</label>
                                <textarea name="notes" class="form-control" rows="4" id="notes">{{$customer->notes}}</textarea>
                            </div>                            
                            <button type="submit" class="btn btn-default">Sửa</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection