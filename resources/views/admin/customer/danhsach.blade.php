@extends('admin.layout.index')

@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Khách hàng / Customer
                            <small>Danh sách</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if (session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Tên khách hàng</th>
                                <th>Giới tính</th>
                                <th>Email</th>
                                <th style="text-align: center;">Địa chỉ</th>
                                <th>Số điện thoại</th>
                                <th>Chú ý</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($customer as $key)
                            <tr class="odd gradeX" align="center">
                                <td>{{$key->id}}</td>
                                <td>{{$key->ten}}</td>
                                <td>{{$key->gioitinh}}</td>
                                <td>{{$key->email}}</td>
                                <td>{{$key->diachi}}</td>                                
                                <td>{{$key->sodienthoai}}</td>                                
                                <td>{{$key->notes}}</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/customer/xoa/{{$key->id}}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/customer/sua/{{$key->id}}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection