<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    //
    protected $table="Feedback";

    public function book(){
        return $this->belongsTo('App\Book', 'idBook', 'id');
    }
    public function user(){
        return $this->belongsTo('App\User', 'idUser', 'id');
    }
}
