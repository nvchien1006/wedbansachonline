<?php

namespace App\Http\Controllers;

use App\Author;
use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class AuthorController extends Controller
{
    //
    public function getCreateAuthor(){
        return view('admin.author.them');
    }
    public function postCreateAuthor(Request $request){
        $this->validate($request, [
            'ten' => 'required|unique:author,ten',
            'hinh' => 'required',
            'mota' => 'required'
        ], [
            'ten.required' => 'Bạn chưa nhập tên Tác giả',
            'ten.unique' => 'Tên tác giả đã được sử dụng',
            'hinh.required' => 'Bạn chưa thêm ảnh',
            'mota.required'=>'Bạn chưa thêm mô tả'
        ]);

        $author = new Author();
        $author->ten= $request->ten;
        $author->tenkhongdau = changeTitle($request->ten);
        $author->mota = $request->mota;

        if ($request->hasFile('hinh')) {
            $file = $request->file('hinh');
            $savefile = time() . '.' . $file->getClientOriginalExtension();
            $file->move(Config::get('MyConst.author_img_folder'), $savefile);
            $author->hinh = $savefile;
        }else{
            $author->hinh = "";
        }
        $author->save();

        return redirect()->back()->with('thongbao', 'Thêm thành công');

    }

    public function danhsachAuthor(){
        $authors = Author::all();
        return view('admin.author.danhsach')->with('author',$authors);
    }

    public function xoaAuthor($id){

        $author = Author::find($id);
        $author->delete();
        return redirect()->back()->with('thongbao', 'Xóa thành công');

    }

    public function getEditAuthor($id){
        $author = Author::find($id);
        return view('admin.author.sua')->with('author', $author);
    }

    public function postEditAuthor(Request $request,$id)
    {
        $this->validate($request,[
            'ten'=>'required|min:3',
            'mota'=>'required',
        ],[
            'ten.required'=>'Bạn chưa nhập tên',
            'ten.unique'=>'Tên tác giả đã tồn tại',
            'ten.min'=>'Tên tác giả ít nhất 3 ký tự',
            'mota.required' => 'Bạn chưa nhập mô tả',
        ]);

        $author = Author::find($id);
        $author->ten = $request->ten;
        $author->tenkhongdau = changeTitle($request->ten);
        $author->mota = $request->mota;

        //
        if ($request->hasFile('hinh')) {
            $file = $request->file('hinh');
            $newFileName = time() . '.' . $file->getClientOriginalExtension();
            $file->move(Config::get('MyConst.author_img_folder'), $newFileName);
            $author->hinh = $newFileName;
        }
        $author->save();

        return redirect()->back()->with('thongbao','Sửa tác thông tin thành công');

    }

}
