@extends('layout.index')
@section('content')
<div class="inner-header">
		<div class="container">
			<div class="pull-left">
				<h6 class="inner-title">Tìm kiếm : -- {{$key}} --</h6>
			</div>
			<div class="pull-right">
				<div class="beta-breadcrumb font-large">
					<a href="trangchu">Home</a> / <span>Sản phẩm tìm kiếm</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="container">
		<div id="content" class="space-top-none">
			<div class="main-content">
				<div class="space60">&nbsp;</div>
				<div class="row">
					@include('layout.menu')
					<div class="col-sm-9">
						<div class="beta-products-list">
							<h4>Sản phẩm</h4>
							<div class="beta-products-details">
								<p class="pull-left">Tìm thấy {{count($timkiems)}} sản phẩm</p>
								<div class="clearfix"></div>
							</div>

							<div class="row">
								@foreach ($timkiems as $key)
								<div class="col-sm-4">
                                    <div class="space10">&nbsp;</div>
                                    <div class="single-item">
										<div class="single-item-header">
											<a href="chitietsanpham/{{$key->id}}"><img src="source/upload/book/{{$key->hinh}}" alt="" width="200px" height="250px"></a>
										</div>
										<div class="single-item-body">
											<p class="single-item-title">{{$key->ten}}</p>
											<p class="single-item-price">
												<span>{{number_format($key->giatien)}} VNĐ</span>
											</p>
										</div>
										<div class="single-item-caption">
											<a class="add-to-cart pull-left" href="themgiohang/{{$key->id}}"><i class="fa fa-shopping-cart"></i></a>
											<a class="beta-btn primary" href="chitietsanpham/{{$key->id}}">Chi tiết <i class="fa fa-chevron-right"></i></a>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
								@endforeach
							</div>
							<div class="row"> {{$timkiems->links()}}</div>
						</div> <!-- .beta-products-list -->

						<div class="space50">&nbsp;</div>
					</div>
				</div> <!-- end section with sidebar and main content -->


			</div> <!-- .main-content -->
		</div> <!-- #content -->
@endsection
