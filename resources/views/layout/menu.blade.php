<div class="col-sm-3">
	<ul class="aside-menu">
		<li><span style="font-weight: bold">Thể loại</span></li>
		<ul>
			@foreach ($typebook as $key)
                <li><a href="loaisanpham/{{$key->id}}">{{$key->ten}}</a></li>
			@endforeach
		</ul>
		<li><span style="font-weight: bold">Tác giả</span></li>
		<ul>
			@foreach ($author as $key)
                <li><a href="tacgia/{{$key->id}}">{{$key->ten}}</a></li>
			@endforeach
		</ul>
		<li><span style="font-weight: bold">Nhà xuất bản</span></li>
		<ul>
			@foreach ($publisher as $key)
                <li><a href="nhasanxuat/{{$key->id}}">{{$key->ten}}</a></li>
			@endforeach
		</ul>
	</ul>
</div>
