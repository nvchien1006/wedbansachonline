@extends('layout.index')
@section('content')
    <div class="inner-header">

        <div class="container">
            <div class="pull-left">
                <h6 class="inner-title">Liên hệ</h6>
            </div>

            <div class="pull-right">
                <div class="beta-breadcrumb font-large">
                    <a href="trangchu">Trang chủ</a> / <span>Liên hệ</span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

    </div>
    <div class="beta-map">

        <div class="abs-fullwidth beta-map wow flipInX">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d59594.78648258675!2d105.8083277904113!3d21.00569484723514!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBCw6FjaCBraG9hIEjDoCBO4buZaSwgxJDhuqFpIEPhu5MgVmnhu4d0LCBIw6AgTuG7mWksIFZp4buHdCBOYW0!3m2!1d21.0056183!2d105.8433475!5e0!3m2!1svi!2s!4v1507777064322"
                width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
    <div class="container">
        <div id="content" class="space-top-none">
            <div class="space50">&nbsp;</div>
            <div class="row">
                <div class="col-sm-4">
                    <h2>Thông tin liên hệ</h2>
                    <div class="space20">&nbsp;</div>

                    <h6 class="contact-title">Địa chỉ : </h6><br>

                    <p class="contact-title">Trường đại học Bách Khoa Hà Nội</p><br>
                    <p>
                        Số 1, Đại Cồ Việt, Hai Bà Trưng, Hà Nội, ViệtNam
                    </p>
                    <div class="space20">&nbsp;</div>
                    <h6 class="contact-title"> Email :
                        <a href="mailto:nvchien1006@gmail.com"> nvchien1006@gmail.com</a>
                    </p>
                    <div class="space20">&nbsp;</div>
                </div>
            </div>
        </div> <!-- #content -->
    </div> <!-- .container -->
@endsection
