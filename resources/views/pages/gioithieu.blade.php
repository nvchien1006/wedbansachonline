@extends('layout.index')
@section('content')
    <div class="inner-header">
        <div class="container">
            <div class="pull-left">
                <h6 class="inner-title">Giới thiệu</h6>
            </div>
            <div class="pull-right">
                <div class="beta-breadcrumb font-large">
                    <a href="trangchu">Trang chủ</a> / <span>Giới thiệu</span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="container">
        <div id="content">
            <div class="our-history">
                <h2 class="text-center wow fadeInDown">Lịch sử</h2>
                <div class="space35">&nbsp;</div>

                <div class="history-slider">
                    <div class="history-navigation">
                        <a data-slide-index="0" href="blog_with_2sidebars_type_e.html" class="circle"><span class="auto-center">2018</span></a>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <div class="col-sm-7">
                            <h5 class="other-title">Ngày tạo</h5>
                            <p>
                                1/1/2018<br>
                                Trường đại học Bách Khoa Hà Nội<br>
                                Viện CNTT và Truyền thông<br>
                            </p>
                            <div class="space20">&nbsp;</div>
                            <p>Trang web bán sách online <br>
                                Nguyễn Văn Chiến<br>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- #content -->
    </div> <!-- .container -->
@endsection
