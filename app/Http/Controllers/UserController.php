<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class UserController extends Controller
{
    //
    public function listUser(){
        $user=User::all();
        return view('admin.user.danhsach', ['user'=>$user]);
    }

    public function getAddUser(){
        return view('admin.user.them');
    }

    public function postAddUser(Request $request){
        $this->validate($request,
            [
                'name'=>'required|min:3',
                'email'=>'required|email|unique:users,email',
                'password'=>'required|min:6|max:32',
                'passwordAgain'=>'required|same:password'
            ],
            [
                'name.required'=>'Bạn chưa nhập tên người dùng',
                'name.min'=>'Tên người dùng có ít nhất 3 kí tự',
                'email.required'=>'Bạn chưa nhập Email',
                'email.email'=>'Bạn chưa nhập đúng định dạng Email',
                'email.unique'=>'Email đã tồn tại',
                'password.required'=>'Bạn chưa nhập mật khẩu',
                'password.min'=>'Mật khẩu có ít nhất 6 kí tự đến 32 kí tự',
                'password.max'=>'Mật khẩu có ít nhất 6 kí tự đến 32 kí tự',
                'passwordAgain.required'=>'Bạn chưa nhập lại mật khẩu',
                'passwordAgain.same'=>'Mật khẩu không trùng khớp'
            ]);

        $user=new User;
        $user->name=$request->name;
        $user->tendangnhap=$request->tendangnhap;
        $user->email=$request->email;
        $user->password=bcrypt($request->password);

        if ($request->hasFile('hinh')) {
            $file = $request->file('hinh');
            $newFileName = time() . '.' . $file->getClientOriginalExtension();
            $file->move(Config::get('MyConst.slide_img_folder'), $newFileName);
            $user->hinh = $newFileName;
        }else{
            $user->hinh = "";
        }
        $user->save();

        return redirect('admin/user/them')->with('thongbao', 'Thêm thành công');
    }

    public function getEditUser($id){
        $user=User::find($id);
        return view('admin.user.sua', ['user'=>$user]);
    }

    public function postEditUser(Request $request, $id){
        $this->validate($request,
            [
                'name'=>'required|min:3',
            ],
            [
                'name.required'=>'Bạn chưa nhập tên người dùng',
                'name.min'=>'Tên người dùng có ít nhất 3 kí tự',
            ]);

        $user=User::find($id);

        $user->name=$request->name;
        $user->tendangnhap=$request->tendangnhap;
        $user->quyen=$request->quyen;

        if ($request->changePassword == "on") {
            $this->validate($request,
                [
                    'password'=>'required|min:6|max:32',
                    'passwordAgain'=>'required|same:password'
                ],
                [
                    'password.required'=>'Bạn chưa nhập mật khẩu',
                    'password.min'=>'Mật khẩu có ít nhất 6 kí tự đến 32 kí tự',
                    'password.max'=>'Mật khẩu có ít nhất 6 kí tự đến 32 kí tự',
                    'passwordAgain.required'=>'Bạn chưa nhập lại mật khẩu',
                    'passwordAgain.same'=>'Mật khẩu không trùng khớp'
                ]);
            $user->password = bcrypt($request->password);
        }

        //
        if ($request->hasFile('hinh')) {
            $file = $request->file('hinh');
            $newFileName = time() . '.' . $file->getClientOriginalExtension();
            $file->move(Config::get('MyConst.user_img_folder'), $newFileName);
            $user->hinh = $newFileName;
        }
        $user->save();

        return redirect('admin/user/sua/'.$id)->with('thongbao', 'Sửa thành công');
    }

    public function xoaUser($id){
        $user=User::find($id);
        if ($user != null){
            $user->delete();
            return redirect()->back()->with('thongbao', 'Xóa thành công');
        }
        return redirect()->back()->with('thongbao', 'ID NULL');
    }
}
