<?php

namespace App\Http\Controllers;

use App\Bill;
use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{

    //
    public function getAddCustomer(){
        return view('admin.customer.them');
    }

    public function postAddCustomer(Request $request){
        $this->validate($request,
            [
                'ten'=>'required',
                'gioitinh'=>'required',
                'email'=>'required|email|unique:customer,email',
                'diachi'=>'required',
                'sodienthoai'=>'required',
            ],
            [
                'ten.required'=>'Bạn chưa nhập tên khách hàng',
                'gioitinh.required'=>'Bạn cần điền giới tính',
                'email.required'=>'Bạn cần nhập địa chỉ email',
                'email.email'=>'Định dạng emial chưa đúng',
                'email.unique'=>'Email đã tồn tại',
                'diachi.required'=>'Bạn chưa nhập địa chỉ',
                'sodienthoai.required'=>'Bạn chưa nhập số điện thoại',
            ]);

        $customer = new Customer();
        $customer->ten =$request->ten;
        $customer->gioitinh =$request->gioitinh;
        $customer->email =$request->email;
        $customer->diachi = $request->diachi;
        $customer->sodienthoai = $request->sodienthoai;
        $customer->notes = $request->notes;
        $customer->save();

        return redirect()->back()->with('thongbao', 'Thêm thành công');
    }

    //
    public function listCustomer(){
        $customer = Customer::all();
        return view('admin.customer.danhsach')->with('customer',$customer )
            ;
    }

//
    public function getEditCustomer($id){
        $customer  = Customer::find($id);

        return view('admin.customer.sua')->with('customer',$customer);
    }

    public function postEditCustomer(Request $request,$id){
        $this->validate($request,
            [
                'ten'=>'required',
                'gioitinh'=>'required',
                'diachi'=>'required',
                'sodienthoai'=>'required',
            ],
            [
                'ten.required'=>'Bạn chưa nhập tên khách hàng',
                'gioitinh.required'=>'Bạn cần điền giới tính',
                'diachi.required'=>'Bạn chưa nhập địa chỉ',
                'sodienthoai.required'=>'Bạn chưa nhập số điện thoại',
            ]);

        $customer =Customer::find($id);
        $customer->ten =$request->ten;
        $customer->gioitinh =$request->gioitinh;
        $customer->diachi = $request->diachi;
        $customer->sodienthoai = $request->sodienthoai;
        $customer->notes = $request->notes;
        $customer->save();

        return redirect()->back()->with('thongbao', 'Sửa thành công');
    }


    public function xoaCustomer($id){
        $customer = Customer::find($id);
        $customer->delete();
        return redirect()->back()->with('thongbao',"Xóa thông tin khách hàng thành công");

    }
}
