<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('idUser');
            $table->unsignedBigInteger('idBook');
            $table->string('noidung');

            $table->foreign('idUser')->references('id')->on('users')->onDelete('cascade');;
            $table->foreign('idBook')->references('id')->on('book')->onDelete('cascade');;

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback');
    }
}
