<?php

namespace App\Providers;

use App\Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        view()->composer(['layout.headerr', 'pages.dathang'], function ($view){
            if (Session('cart')) {
                $oldCart = Session::get('cart');
                $cart = new Cart($oldCart);
                $view->with(['cart'=>Session::get('cart'),'book_cart'=>$cart->items,
                    'totalPrice'=>$cart->totalPrice, 'totalQty'=>$cart->totalQty,'shipfee'=>$cart->getAllShipFee()]);
            }
        });

        view()->composer(['*'], function ($view) {
            if (Auth::check()) {
                view()->share('user_login', Auth::user());
            }
        });
    }
}
