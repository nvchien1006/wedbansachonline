<?php

namespace App\Http\Controllers;

use App\Bill;
use App\Bills;
use App\Book;
use App\DetailBill;
use Illuminate\Http\Request;

class BillDetailController extends Controller
{
    //
    public function listDetailBill()
    {
        $detailbill = DetailBill::all();
        return view('admin.detailbill.danhsach', ['detailbill' => $detailbill]);
    }

//
    public function getAddBilldetail()
    {
        $bill = Bill::all();
        $book = Book::all();
        return view('admin.detailbill.them')->with(['bill' => $bill, 'book' => $book]);
    }


    public function postAddBilldetail(Request $request)
    {
        $this->validate($request,
            [
                'idBill' => 'required',
                'idBook' => 'required',
                'soluong' => 'required|integer',
            ],
            [
                'idBill.required' => 'Bạn chưa chọn Bill',
                'idBook.required' => 'Bạn chưa chọn sách',
                'soluong.required' => 'Bạn chưa nhập số lượng',
                'soluong.min' => 'Số lượng ít nhất là 1',
            ]);

        if ($request->soluong <= 0) {
            return redirect()->back()->with('thongbao', 'Số lượng phải lớn hơn 0');
        }

        $detailbill = new DetailBill;
        $book = Book::find($request->idBook);
        $detailbill->idBook = $request->idBook;
        $detailbill->idBill = $request->idBill;
        $detailbill->soluong = $request->soluong;
        $detailbill->giatien = $book->giatien + $book->phigiaohang;

        $detailbill->save();

        return redirect()->back()->with('thongbao', 'Thêm thành công');
    }

//
    public function getEditBilldetail($id)
    {
        $bill = Bill::all();
        $detailbill = DetailBill::find($id);
        $book = Book::all();
        return view('admin.detailbill.sua')->with(['bill' => $bill, 'book' => $book, 'detailbill' => $detailbill]);
    }

//
    public function postEditBilldetail(Request $request, $id){
        $this->validate($request,
            [
                'idBill' => 'required',
                'idBook' => 'required',
                'soluong' => 'required|integer',
            ],
            [
                'idBill.required' => 'Bạn chưa chọn Bill',
                'idBook.required' => 'Bạn chưa chọn sách',
                'soluong.required' => 'Bạn chưa nhập số lượng',
                'soluong.min' => 'Số lượng ít nhất là 1',
            ]);

        if ($request->soluong <= 0) {
            return redirect()->back()->with('thongbao', 'Số lượng phải lớn hơn 0');
        }

        $detailbill = DetailBill::find($id);
        $book = Book::find($request->idBook);
        $detailbill->idBook = $request->idBook;
        $detailbill->idBill = $request->idBill;
        $detailbill->soluong = $request->soluong;
        $detailbill->giatien = $book->giatien + $book->phigiaohang;

        $detailbill->save();

        return redirect()->back()->with('thongbao', 'Sửa thành công');
    }
//
    public function xoaBilldetail($id)
    {
        $detailbill = DetailBill::where('id', $id);
        if ($detailbill != null) {
            $detailbill->delete();
            return redirect()->back()->with('thongbao', 'Xóa thành công');
        }
        return rediryect()->back()->with('thongbao', 'ID NULL');
    }
}
