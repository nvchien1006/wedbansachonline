<div id="header">
    <div class="header-top">
        <div class="container">
            <div class="pull-left auto-width-left">
                <ul class="top-menu menu-beta l-inline">
                    <li><a href="https://soict.hust.edu.vn/"><i class="fa fa-home"></i> Trường đại học Bách Khoa Hà Nội
                            - Viện CNTT & TT</a></li>
                </ul>
            </div>
            <div class="pull-right auto-width-right">
                <ul class="top-details menu-beta l-inline">
                    @if (Auth::check())
                        <li><a href="upload/them">Upload</a></li>
                        <li><a href="suathongtincanhan"><i class="fa fa-user"></i>{{Auth::user()->name}}</a></li>
                        <li><a href="dangxuat">Đăng xuất</a></li>
                    @else
                        <li><a href="dangky">Đăng kí</a></li>
                        <li><a href="dangnhap">Đăng nhập</a></li>
                    @endif
                </ul>
            </div>
            <div class="clearfix"></div>
        </div> <!-- .container -->
    </div> <!-- .header-top -->
    <div class="header-body">
        <div class="container beta-relative">
            <div class="pull-left">
                <a href="trangchu" id="logo"><img src="source/assets/dest/images/logo_websach.png" width="200px" alt=""></a>
            </div>
            <div class="pull-right beta-components space-left ov">
                <div class="space10">&nbsp;</div>
                <div class="beta-comp">
                    <form method="post" id="searchform" action="{{route('timkiem')}}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="text" value="" name="timkiem" id="timkiem" placeholder="Nhập từ khóa..."/>
                        <button class="fa fa-search" type="submit" id="searchsubmit"></button>
                    </form>
                </div>

                <div class="beta-comp">
                    @if (Session::has('cart'))
                        <div class="cart">
                            <div class="beta-select"><i class="fa fa-shopping-cart"></i> Giỏ hàng ( @if (Session::has('cart')) {{Session('cart')->totalQty}} @else Trống @endif) <i class="fa fa-chevron-down"></i></div>
                            <div class="beta-dropdown cart-body ">
                                @foreach ($book_cart as $key)
                                    <div class="cart-item">
                                        <a class="cart-item-delete" href="xoagiohang/{{$key['item']['id']}}"><i class="fa fa-times"></i></a>
                                        <div class="media">
                                            <a class="pull-left" href="#"><img src="source/upload/book/{{$key['item']['hinh']}}" alt=""></a>
                                            <div class="media-body">
                                                <span class="cart-item-title">{{$key['item']['ten']}}</span>
                                                <span class="cart-item-amount">{{$key['qty']}} * <span>{{number_format($key['item']['giatien'])}}</span></span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                                <div class="cart-caption">
                                    <div class="cart-total text-right">Tổng tiền hàng: <span class="cart-total-value">{{number_format(Session('cart')->totalPrice)}} VNĐ</span></div>
                                    <div class="clearfix"></div>

                                    <div class="center">
                                        <div class="space10">&nbsp;</div>
                                        <a href="dathang" class="beta-btn primary text-center">Đặt hàng <i class="fa fa-chevron-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- .cart -->
                    @endif
                </div>
            </div>
            <div class="clearfix"></div>
        </div> <!-- .container -->
    </div> <!-- .header-body -->
    <div class="header-bottom" style="background-color: #0277b8;">
        <div class="container">
            <a class="visible-xs beta-menu-toggle pull-right" href="#"><span class='beta-menu-toggle-text'>Menu</span>
                <i class="fa fa-bars"></i></a>
            <div class="visible-xs clearfix"></div>
            <nav class="main-menu">
                <ul class="l-inline ov">
                    <li><a href="trangchu">Trang chủ</a></li>
                    <li><a>Thể loại</a>
                        <ul class="sub-menu pre-scrollable"style="overflow-y: auto;">
                            @foreach ($typebook as $key)
                                <li><a href="loaisanpham/{{$key->id}}">{{$key->ten}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    <li><a>Tác giả</a>
                        <ul class="sub-menu pre-scrollable"style="overflow-y: auto;">
                            @foreach ($author as $key)
                                <li><a href="tacgia/{{$key->id}}">{{$key->ten}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    <li><a>Nhà xuất bản</a>
                        <ul class="sub-menu pre-scrollable"style="overflow-y: auto;">
                            @foreach ($publisher as $key)
                                <li><a href="nhasanxuat/{{$key->id}}">{{$key->ten}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    <li><a href="gioithieu">Giới thiệu</a></li>
                    <li><a href="lienhe">Liên hệ</a></li>
                </ul>
                <div class="clearfix"></div>
            </nav>
        </div> <!-- .container -->
    </div> <!-- .header-bottom -->
</div> <!-- #header -->
