-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2020 at 04:35 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_bansach`
--

-- --------------------------------------------------------

--
-- Table structure for table `author`
--

CREATE TABLE `author` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ten` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenkhongdau` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hinh` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mota` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `author`
--

INSERT INTO `author` (`id`, `ten`, `tenkhongdau`, `hinh`, `mota`, `created_at`, `updated_at`) VALUES
(5, 'Luca Novelli', 'luca-novelli', 'Nxrwz1FBcg_luca_novelli_quadrato-300x300.jpg', 'Luca Novelli (born 7 October 1947) is an Italian cartoonist and writer.\r\n\r\nBorn in Milan, Novelli graduated in agronomy and started drawing his first strips in 1967 for some university publications. He made his professional debut in 1971 with the comics series Historiettes, published in the magazine Eureka, and shortly later he created the successful comic strip Gli Edenisti, published in the newspapers Gazzetta di Parma and Il Messaggero and in the magazine Pardon. In 1974 he created his best known work, the comic strip Il Laureato, published in the newspapers Il Giorno and Il Messaggero and in the magazine Men Only. In 1976 he started collaborating with the German magazine Pardon.[1][2]\r\n\r\nIn 1978 Novelli released Viaggio al centro della cellula, a biology comic textbook, and following its success he specialized in popular science books for children, published in Italy by Mondadori and often translated in foreign languages.[1]', '2020-02-05 06:42:03', '2020-02-05 06:42:03'),
(6, 'Paulo Coelho', 'paulo-coelho', 's7cSP9WwBg_240px-Coelhopaulo26012007-1.jpg', 'Paulo Coelho sinh tại Rio de Janeiro (Brasil), vào trường luật ở đó, nhưng đã bỏ học năm 1970 để du lịch qua México, Peru, Bolivia và Chile, cũng như châu Âu và Bắc Phi. Hai năm sau ông trở về Brasil và bắt đầu soạn lời cho nhạc pop. Ông cộng tác với những nhạc sĩ pop như Raul Seixas. Năm 1974 ông bị bắt giam một thời gian ngắn vì những hoạt động chống lại chế độ độc tài lúc đó ở Brasil.\r\n\r\nSách của ông đã được bán ra hơn 86 triệu bản trên 150 nước và được dịch ra 56 thứ tiếng. Ông đã nhận được nhiều giải thưởng của nhiều nước, trong đó tác phẩm Veronika quyết chết (Veronika decide morrer) được đề cử cho Giải Văn chương Dublin IMPAC Quốc tế có uy tín.\r\n\r\nTiểu thuyết Nhà giả kim (O Alquimista) của ông, một câu chuyện thấm đẫm chất thơ, đã bán được hơn 65 triệu bản trên thế giới và được dịch ra 56 thứ tiếng, trong đó có tiếng Việt.[1] Tác phẩm này được dựng thành phim do Lawrence Fishburne sản xuất, vì diễn viên này rất hâm mộ Coelho. Sách của ông còn có Hành hương (O diário de um mago) (được công ty Arxel Tribe lấy làm cơ sở cho một trò chơi vi tính), Bên sông Piedra tôi ngồi xuống và khóc (Na margem do rio Piedra eu sentei e chorei) và Những nữ chiến binh (As Valkírias). Cuốn tiểu thuyết năm 2005 O Zahir của ông bị cấm ở Iran, 1000 bản sách bị tịch thu [1], nhưng sau đó lại được phát hành.\r\n\r\nNhững sách của ông đã được vào các danh sách những quyển sách bán chạy nhất ở nhiều nước, kể cả Brasil, Anh, Hoa Kỳ, Pháp, Đức, Canada, Ý, Israel và Hy Lạp. Ông là tác giả viết tiếng Bồ Đào Nha bán chạy nhất mọi thời đại.\r\n\r\nMặc dù có nhiều thành công, nhiều nhà phê bình ở Brasil vẫn nhìn ông như một tác giả không quan trọng, cho rằng những tác phẩm của ông quá đơn giản và giống sách tự lực (self-help book). Cũng có người xem các tiểu thuyết của ông có quá nhiều tính chất \"thương mại\". Sự kiện ông được vào Viện Hàn lâm Văn chương Brasil (ABL) đã gây ra nhiều tranh cãi trong các độc giả Brasil và ngay trong chính Viện Hàn lâm.\r\n\r\nÔng và vợ là Christina định cư tại Rio de Janeiro (Brasil) và Tarbes (Pháp).', '2020-02-05 06:43:02', '2020-02-05 06:43:02'),
(7, 'Nora Roberts', 'nora-roberts', 'T2wUEsazrl_f.jpg', 'NoraRobert', '2020-02-05 10:23:09', '2020-02-04 17:00:00'),
(9, 'Kim Dung', 'kim-dung', 'Sxu2Il3pJ1_20161212-053230-kim-dung_520x520.jpg', 'Kim Dung', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(10, 'Tony ', 'tony', '54Uw9QdUs5_tony-bennett-9926699-1-402.jpg', 'Tony', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(11, 'Richard Calson', 'richard-calson', '5e9Z0qm7tw_Richard_Carlson_1952.JPG', 'Richard Calson', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(12, 'Adam Khoo', 'adam-khoo', 'Ol3Kt9fEQw_52cAdam-Khoo--Copy-d7642.jpg', 'Adam Khoo', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(13, 'Diana Gabaldon', 'diana-gabaldon', 'mN2OX3gkIo_Diana_Gabaldon_2009.jpg', 'Sinh: 11 tháng 1, 1952 (tuổi 65), Williams, Arizona, Hoa Kỳ\r\nVợ/chồng: Doug Watkins\r\nHọc vấn: Northern Arizona University\r\nGiải thưởng: Giải Sự lựa chọn của Goodreads cho Sách thể loại lãng mạn hay nhất', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(17, 'Chris Bradford', 'chris-bradford', 'oenq0rVs8Z_Chris Bradford.jpg', 'Chưa có thông tin về tác giả', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(18, 'Andrew Roberts', 'andrew-roberts', 'xCVnY3awE9_Andrew Roberts.jpg', 'Sinh: 13 tháng 1, 1963 (tuổi 54), Luân Đôn, Vương quốc Liên hiệp Anh và Bắc Ireland\r\nHọc vấn: Gonville and Caius College, Cambridge (1985)', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(19, 'Cay S. Horstmann', 'cay-s-horstmann', 'dkDjr5MkA6_CayHorstmann.jpg', 'Sinh: 16 tháng 6, 1959 (tuổi 58)\r\n', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(20, ' kristina halvorson', 'kristina-halvorson', 'FKW2TjHgN2_a.jpg', 'Chưa có thông tin về tác giả', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(21, 'Jo Gang – Soo', 'jo-gang-?-soo', 'wmWdK8vpkc_260806.jpg', 'Jo Gang – Soo', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(22, 'Tô Hoài', 'to-hoai', 'xUOak5IOKG_20140707074227unnamed.jpg', 'Tô Hoài (tên khai sinh: Nguyễn Sen; 27 tháng 9 năm 1920 – 6 tháng 7 năm 2014) là một nhà văn Việt Nam. Một số tác phẩm đề tài thiếu nhi của ông được dịch ra ngoại ngữ. Ông được nhà nước Việt Nam trao tặng Giải thưởng Hồ Chí Minh về Văn học - Nghệ thuật Đợt 1 (1996) cho các tác phẩm: Xóm giếng, Nhà nghèo, O chuột, Dế mèn phiêu lưu ký, Núi Cứu quốc, Truyện Tây Bắc, Mười năm, Xuống làng, Vỡ tỉnh, Tào lường, Họ Giàng ở Phìn Sa, Miền Tây, Vợ chồng A Phủ, Tuổi trẻ Hoàng Văn Thụ.', '2020-02-04 17:00:00', '2020-02-04 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

CREATE TABLE `bills` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idCustomer` bigint(20) UNSIGNED NOT NULL,
  `ngaydonhang` date NOT NULL,
  `tongtien` double(8,2) NOT NULL,
  `trangthai` int(11) NOT NULL,
  `notes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bills`
--

INSERT INTO `bills` (`id`, `idCustomer`, `ngaydonhang`, `tongtien`, `trangthai`, `notes`, `created_at`, `updated_at`) VALUES
(3, 7, '2020-02-05', 249000.00, 0, NULL, '2020-02-04 20:15:05', '2020-02-04 20:15:05');

-- --------------------------------------------------------

--
-- Table structure for table `bill_detail`
--

CREATE TABLE `bill_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idBill` bigint(20) UNSIGNED NOT NULL,
  `idBook` bigint(20) UNSIGNED NOT NULL,
  `soluong` int(11) NOT NULL,
  `giatien` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bill_detail`
--

INSERT INTO `bill_detail` (`id`, `idBill`, `idBook`, `soluong`, `giatien`, `created_at`, `updated_at`) VALUES
(4, 3, 7, 1, 20000.00, '2020-02-04 20:15:05', '2020-02-04 20:15:05'),
(5, 3, 3, 2, 60000.00, '2020-02-04 20:15:05', '2020-02-04 20:15:05'),
(6, 3, 8, 1, 20000.00, '2020-02-04 20:15:05', '2020-02-04 20:15:05'),
(7, 3, 9, 1, 79000.00, '2020-02-04 20:15:05', '2020-02-04 20:15:05');

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ten` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenkhongdau` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mota` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sotrang` int(11) NOT NULL,
  `giatien` int(11) NOT NULL,
  `hinh` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `soluottruycap` int(11) NOT NULL,
  `trangthai` tinyint(4) NOT NULL,
  `idUser` bigint(20) UNSIGNED NOT NULL,
  `idLoaisach` bigint(20) UNSIGNED NOT NULL,
  `idTacgia` bigint(20) UNSIGNED NOT NULL,
  `idNhasanxuat` bigint(20) UNSIGNED NOT NULL,
  `phigiaohang` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `ten`, `tenkhongdau`, `mota`, `sotrang`, `giatien`, `hinh`, `soluottruycap`, `trangthai`, `idUser`, `idLoaisach`, `idTacgia`, `idNhasanxuat`, `phigiaohang`, `created_at`, `updated_at`) VALUES
(3, 'Galileo Và Hành Trình Đến Các Vì Sao', 'galileo-va-hanh-trinh-den-cac-vi-sao', 'Galileo Và Hành Trình Đến Các Vì Sao', 300, 60000, 'uJgreX4dqb_galioleo.PNG', 10, 1, 1, 3, 5, 6, 0, '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(4, 'Nhà giả kim', 'nha-gia-kim', 'Nhà giả kim là một cuốn sách được xuất bản lần đầu ở Brasil năm 1988 và là cuốn sách nổi tiếng nhất của nhà văn Paulo Coelho. Nó được dịch ra 67 ngôn ngữ và bán ra tới 65 triệu bản, trở thành một trong những quyển sách bán chạy nhất mọi thời đại.', 225, 59000, 'TTYp6KiFlJ_img117.u3059.d20170616.t100547.729023.jpg', 20, 1, 1, 2, 6, 5, 10000, '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(5, 'Island Of Glass', 'island-of-glass', 'Island Of Glass của Roberts', 300, 80000, '2btezgbcXV_book1.PNG', 19, 0, 1, 2, 7, 5, 0, '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(7, 'Thiên Long Bát Bộ', 'thien-long-bat-bo', 'Thiên Long Bát Bộ của Kim Dung', 75, 20000, '5Rr0rUqg17_book3.PNG', 10, 1, 1, 5, 9, 5, 0, '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(8, 'Tiếu ngạo giang hồ', 'tieu-ngao-giang-ho', 'Tiếu ngạo giang hồ của Kim Dung', 76, 20000, 'GE8ftwuG46_book4.PNG', 22, 1, 1, 5, 9, 5, 0, '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(9, 'Trên đường băng', 'tren-duong-bang', 'Trên đường băng của Tony', 278, 79000, 'x8Y8Rd3CEK_book5.PNG', 32, 1, 1, 6, 10, 4, 10000, '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(11, 'Tôi tài giỏi bạn cũng thế', 'toi-tai-gioi-ban-cung-the', 'Tôi tài giỏi, bạn cũng thế! là quyển sách bán chạy nhất của doanh nhân người Singapore Adam Khoo, viết về những phương pháp học tập tiên tiến. Quyển sách đã được dịch ra hàng chục thứ tiếng, trong đó Tôi tài giỏi, bạn cũng thế!', 400, 109000, '5dAnMVGVjN_unnamed.png', 30, 1, 1, 6, 12, 7, 10000, '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(12, 'Outlander - Chuồn chuồn hổ phách 1', 'outlander-chuon-chuon-ho-phach-1', '\"Outlander-Chuồn chuồn hổ phách là cuốn thứ hai trong bộ tiểu thuyết lịch sử xuyên thời gian của Diana Gabaldon. Bà đã dệt nên một câu chuyện đầy ma lực đan xen giữa lịch sử và thần thoại, nó chứa đầy những đam mê mãnh liệt và sự táo bạo.\r\n\r\nVô tình bị kéo về quá khứ, Claire - một nữ y tá quân đội người Anh - đã gặp gỡ và kết duyên với Jamie, một chàng chiến binh trẻ người Scot dũng mãnh, hào sảng. Sau nhiều biến cố, họ buộc phải rời nước Anh để sang Pháp. Tại đây, khi biết cuộc nổi dậy của phái Jacobite bắt đầu nhen nhóm, Jamie và Claire quyết định tìm mọi cách để ngăn chặn trận chiến Culloden đẫm máu xảy ra, nhằm bảo vệ sinh mạng của hàng nghìn người Scot vô tội. Thế nhưng thay đổi lịch sử có phải là chuyện dễ dàng? Liệu họ có thể cải biến vận mệnh của cả một dân tộc hay đành chịu cúi đầu khuất phục trước bàn tay tàn nhẫn của số phận?\"', 616, 116000, 'nKVF2Ktoq2_upload_80b8b8a5e2e24f9cb30d86cc87793f9d.jpg', 40, 1, 1, 3, 13, 8, 10000, '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(16, 'SAMURAI TRẺ TUỔI - Bộ 8 tập', 'samurai-tre-tuoi-bo-8-tap', 'Tập 1:VÕ SỸ ĐẠO\r\nTập 2:KIẾM ĐẠO\r\nTập 3:LONG ĐẠO\r\nTập 4:NGŨ ĐẠI-ĐỊA\r\nTập 5:NGŨ ĐẠI - THỦY\r\nTập 6: NGŨ ĐẠI - HỎA\r\nTập 7: NGŨ ĐẠI - PHONG', 2080, 390600, '3WNO0bMK9E_samuraitretuoi-sachkhaitam.jpg', 95, 1, 1, 3, 17, 7, 10000, '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(17, 'Napoleon Đại Đế', 'napoleon-dai-de', 'Napoleon là một nhân vật đặc biệt vĩ đại và hấp dẫn trong lịch sử Pháp cũng như lịch sử thế giới. Cuộc đời, sự nghiệp, quan điểm, tài năng của ông đã là chủ đề của hàng nghìn cuốn sách trong suốt hai thế kỉ qua, và có lẽ sẽ còn được nghiên cứu tiếp trong nhiều thế kỉ sau nữa.\r\n\r\nTrong bối cảnh những cuốn sách dịch ra tiếng Việt về ông cũng như về thời hậu Cách mạng Pháp ở Việt Nam vẫn chỉ đếm trên đầu ngón tay, ít cả về số lượng lẫn tầm vóc, thì cuốn sách \"Napoleon Đại đế\" có thể xem là một hiện tượng bởi tầm vóc, quy mô, tính chất giàu sử liệu đã làm nên giá trị đặc biệt của tác phẩm.', 1220, 364800, 'kB8HNbyHKj_napoleon-dai-de.jpg', 225, 1, 1, 4, 18, 5, 10000, '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(18, 'Core java volume 1', 'core-java-volume-1', 'Cuốn sách viết về Java 8 dành cho những lập trình viên yêu thích ngôn ngữ lập trình java', 1700, 345000, 'hFwXHa2xEr_41dHD22LDJL._SX390_BO1,204,203,200_.jpg', 13, 1, 1, 2, 19, 5, 10000, '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(19, 'Content strategy for the web', 'content-strategy-for-the-web', 'Cuốn sách dành cho những người yêu thích lập trình web', 700, 103000, 'NaYlrUTf6n_41jQ0rWRLjL._SX387_BO1,204,203,200_.jpg', 8, 1, 1, 2, 20, 3, 10000, '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(20, 'LUYỆN THI TOEIC 750 READING', 'luyen-thi-toeic-750-reading', 'Người viết sách không chỉ chú trọng vào nội dung ôn luyện về số lượng đơn thuần mà còn nỗ lực nâng cao chất lượng ôn luyện trong từng bài học. Ngoài ra, giáo trình còn đề cập tới cả những cấu trúc, dấu hiệu dễ gây nhầm lẫn cũng như trình bày một phần riêng về phương pháp đạt điểm cao, để giúp thí sinh giảm thiểu sai sót khi làm bài', 392, 103000, 'iYHqPf2Se6_0.jpg', 1, 0, 1, 2, 21, 3, 10000, '2017-12-09 01:26:20', '2020-02-04 18:01:21');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ten` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gioitinh` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diachi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sodienthoai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `ten`, `gioitinh`, `email`, `diachi`, `sodienthoai`, `notes`, `created_at`, `updated_at`) VALUES
(7, 'Nguyen Van Chien', 'nam', 'admin@gmail.com', 'Bac Ninh', '1234567890', NULL, '2020-02-04 20:15:05', '2020-02-04 20:15:05');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idUser` bigint(20) UNSIGNED NOT NULL,
  `idBook` bigint(20) UNSIGNED NOT NULL,
  `noidung` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `idUser`, `idBook`, `noidung`, `created_at`, `updated_at`) VALUES
(1, 1, 9, 'Hay!!', '2020-02-04 20:13:54', '2020-02-04 20:13:54'),
(2, 1, 8, 'Nội dung hay v~!!!', '2020-02-04 20:14:35', '2020-02-04 20:14:35');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(95, '2014_10_12_000000_create_users_table', 1),
(96, '2014_10_12_100000_create_password_resets_table', 1),
(97, '2019_08_19_000000_create_failed_jobs_table', 1),
(98, '2020_02_04_102817_slide', 1),
(99, '2020_02_04_130555_typebook', 1),
(100, '2020_02_04_140117_create_publisher_table', 1),
(101, '2020_02_04_155203_create_author_table', 1),
(102, '2020_02_04_155402_create_customer_table', 1),
(103, '2020_02_04_155530_create_bill_table', 1),
(104, '2020_02_04_160624_create_book_table', 1),
(105, '2020_02_04_160713_create_feedback_table', 1),
(106, '2020_02_04_160748_create_billdetail_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `publisher`
--

CREATE TABLE `publisher` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ten` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenkhongdau` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hinh` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mota` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `publisher`
--

INSERT INTO `publisher` (`id`, `ten`, `tenkhongdau`, `hinh`, `mota`, `created_at`, `updated_at`) VALUES
(2, 'Nhà xuất bản giáo dục', 'nha-xuat-ban-giao-duc', 'X8OmjNg5ea_83989e14ebd37494.jpg', 'Nhà xuất bản Giáo dục Việt Nam còn gọi là Nhà xuất bản Giáo dục, là một nhà xuất bản được sự quản lý của Nhà nước Việt Nam, trực thuộc Bộ Giáo dục và Đào tạo.\r\n\r\nNhà xuất bản Giáo dục Việt Nam có nhiều chi nhánh và công ty con trên toàn quốc.', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(3, 'Nhà xuất bản ĐHQG Hà Nội', 'nha-xuat-ban-dhqg-ha-noi', '1PHjTO8ir9_10170931_627511177336878_7361769479381628393_n.jpg', 'Tên Công ty/Cửa hàng	:	Trung Tâm Kinh Doanh Xuất Bản Và Phát Hành Sách\r\nĐịa chỉ	:	Ngõ II, Hàng Chuối, Hai Bà Trưng, Hà Nội\r\nĐiện thoại	:	0989633612\r\nEmail	:	vnubooks@gmail.com', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(4, 'Nhà xuất bản thanh niên', 'nha-xuat-ban-thanh-nien', 'DE46NZxT07_images.jpg', 'Nhà xuất bản Thanh Niên\r\n 64 Bà Triệu, Hoàn Kiếm, Hà Nội\r\n chinhanhnxbthanhnien@gmail.com\r\n 0462631724', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(5, 'Nhà xuất bản  tri thức', 'nha-xuat-ban-tri-thuc', '49rOjJVMl6_Logonxb_vuong.png', 'Thông tin cơ bản:\r\n\r\nNhà xuất bản Tri thức\r\n\r\nĐịa chỉ: 53 Nguyễn Du, quận Hai Bà Trưng, Hà Nội, Việt Nam\r\n\r\nĐiện thoại: (84-24) 39454 661 / 3944 7279 / 3944 7280 - Fax: (84-24) 3945 4660\r\n\r\nWebsite: www.nxbtrithuc.com.vn\r\n\r\nCơ quan chủ quản: Liên hiệp các Hội Khoa học và Kỹ thuật Việt Nam (VUSTA)\r\n\r\n---\r\n\r\nThông tin liên hệ chi tiết\r\n\r\n- Phòng Hành chính-Kế toán-Tổng hợp\r\n\r\nPhòng 114 – Tầng 1 - 53 Nguyễn Du - quận Hai Bà Trưng – Hà Nội\r\n\r\nĐiện thoại: 8424 3944 7279 / Fax: 8424 3945 4660\r\n\r\n- Ban Biên tập sách Nhà xuất bản Tri thức\r\n\r\nPhòng 3B11 - Tầng 1 - 53 Nguyễn Du - quận Hai Bà Trưng - Hà Nội\r\n\r\nĐiện thoại: 8424 3944 7278 / Fax: 8424 3945 4660 - Email: banbientap@nxbtrithuc.com.vn\r\n\r\n- Phòng Phát hành\r\n\r\nPhòng 114 - Tầng 1 - 53 Nguyễn Du - quận Hai Bà Trưng - Hà Nội\r\n\r\nĐiện thoại: 8424 3945 4661 / Fax: 8424 3945 4660\r\n\r\nEmail: lienhe@nxbtrithuc.com.vn; lienhenxbtrithuc@gmail.com\r\n\r\n- Ban Truyền thông\r\n\r\nPhòng 3B11 - Tầng 1 - 53 Nguyễn Du - quận Hai Bà Trưng - Hà Nội\r\n\r\nĐiện thoại: 8424 3944 7280 / Fax: 8424 3945 4660\r\n\r\nEmail: nxbtrithuc@gmail.com', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(6, 'Nhà xuất bản Kim Đồng', 'nha-xuat-ban-kim-dong', 'ziw6wNcMkb_nxb_kimdong.png', 'Nhà xuất bản Kim Đồng là nhà xuất bản chuyên sản xuất và phát hành sách, văn hóa phẩm dành cho trẻ em lớn nhất tại Việt Nam với hơn 1.000 đầu sách mỗi năm thuộc nhiều thể loại như văn học, lịch sử, khoa học, truyện tranh,... Bên cạnh việc hợp tác với các cá nhân vá tổ chức trong nước, Nhà xuất bản Kim Đồng còn hợp tác với hơn 70 nhà xuất bản khác trên khắp thế giới, đặc biệt các nhà xuất bản như Dorling Kindersley, HarperCollins UK, Simon and Schuster UK, Dami International, Shogakukan, Nhà xuất bản Seoul,...\r\n\r\nBên cạnh hoạt động xuất bản sách, cơ quan này còn là nhà tổ chức thường xuyên các cuộc vận động sáng tác truyện, truyện tranh, thơ, nhạc, kịch... cho thiếu nhi. Nhà xuất bản Kim Đồng cũng tham gia một số hoạt động xã hội như quỹ học bổng Doraemon, phụng dưỡng bà mẹ Việt Nam anh hùng, xây dựng tủ sách cho xã nghèo và xây trường học cho trẻ em miền núi.\r\n\r\nHiện Nhà xuất bản Kim Đồng có trụ sở chính ở Hà Nội và thành phố Đà Nẵng và Thành phố Hồ Chí Minh.', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(7, 'Nhà xuất bản trẻ', 'nha-xuat-ban-tre', 'g7zfSa2ZtK_1474000299163_8658411.jpg', 'Nhà xuất bản trẻ\r\nSố 21, Dãy A11, Khu Đầm Trấu, Phường Bạch Đằng, Quận Hai Bà Trưng, Hà Nội \r\n(024) 37734544 \r\n(024) 35123395 \r\nchinhanhhanoi@nxbtre.com.vn', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(8, 'NXB Văn Học', 'nxb-van-hoc', 'gSXnniFmm0_nxbvanhoc.jpg', 'NHÀ XUẤT BẢN VĂN HỌC \r\nĐịa chỉ: 18 Nguyễn Trường Tộ, P.Trúc Bạch, Ba Đình, Hà Nội. \r\nĐiện thoại: (043). 7161518 - 7161190 - 7163409    Fax: (04) 38294781\r\nEmail: tonghopvanhoc@vnn.vn', '2020-02-04 17:00:00', '2020-02-04 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `slide`
--

CREATE TABLE `slide` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ten` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hinh` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slide`
--

INSERT INTO `slide` (`id`, `ten`, `hinh`, `link`, `created_at`, `updated_at`) VALUES
(1, 'slide4', '1580864896.png', NULL, '2020-02-04 18:08:16', '2020-02-04 18:08:16'),
(2, 'slide2', '1580864756.jpg', '1', '2020-02-04 18:05:56', '2020-02-04 18:05:56'),
(3, 'slide3', '1580864881.jpg', NULL, '2020-02-04 18:08:01', '2020-02-04 18:08:01'),
(5, 'slide1', '1580864734.jpg', 'null', '2020-02-04 18:05:34', '2020-02-04 18:05:34');

-- --------------------------------------------------------

--
-- Table structure for table `type_book`
--

CREATE TABLE `type_book` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ten` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenkhongdau` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mota` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `type_book`
--

INSERT INTO `type_book` (`id`, `ten`, `tenkhongdau`, `mota`, `created_at`, `updated_at`) VALUES
(2, 'Sách công nghệ thông tin', 'sach-cong-nghe-thong-tin', 'Công nghệ Thông tin, viết tắt CNTT, (tiếng Anh: Information Technology hay là IT) là một nhánh ngành kỹ thuật sử dụng máy tính và phần mềm máy tính để chuyển đổi, lưu trữ, bảo vệ, xử lý, truyền tải và thu thập thông tin.', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(3, 'Sách văn học', 'sach-van-hoc', 'Văn học. ... Văn học là một loại hình sáng tác, tái hiện những vấn đề của đời sống xã hội và con người. Phương thức sáng tạo của văn học được thông qua sự hư cấu, cách thể hiện nội dung các đề tài được biểu hiện qua ngôn ngữ. Khái niệm văn học đôi khi có ', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(4, 'Sách kinh tế', 'sach-kinh-te', 'Kinh tế học là môn khoa học xã hội nghiên cứu sự sản xuất, phân phối và tiêu dùng các loại hàng hóa và dịch vụ. Kinh tế học cũng nghiên cứu cách thức xã hội quản lý các nguồn tài nguyên (nguồn lực) khan hiếm của nó.[1] Nghiên cứu kinh tế học nhằm mục đích', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(5, 'Truyện tranh', 'truyen-tranh', 'Truyện tranh, là những câu chuyện đã xảy ra trong cuộc sống hay những chuyện được tưởng tượng ra được thể hiện qua những bức tranh có hoặc không kèm lời thoại hay các từ ngữ, câu văn kể chuyện.\r\n\r\nHai dòng truyện tranh ảnh hưởng lớn tới mọi dòng truyện tr', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(6, 'Sách kĩ năng', 'sach-ki-nang', 'Có nhiều cách định nghĩa khác nhau về kỹ năng. Những định nghĩa này thường bắt nguồn từ góc nhìn chuyên môn và quan niệm cá nhân của người viết. Tuy nhiên hầu hết chúng ta đều thừa nhận rằng kỹ năng được hình thành khi chúng ta áp dụng kiến thức vào thực ', '2020-02-04 17:00:00', '2020-02-04 17:00:00'),
(7, 'Sách ngoại ngữ', 'sach-ngoai-ngu', 'Ngoại ngữ, được hiểu là Tiếng nước ngoài. Ở Việt Nam, không có khái niệm ngôn ngữ thứ hai, như ở những nước phương Tây. Một số ngoại ngữ phổ biến ở Việt Nam hiện nay như: tiếng Anh, tiếng Pháp, tiếng Nhật, tiếng Trung Quốc, tiếng Hàn Quốc, tiếng Tây Ban N', '2020-02-04 17:00:00', '2020-02-04 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tendangnhap` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quyen` int(11) NOT NULL,
  `lienhe` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hinh` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `tendangnhap`, `email`, `password`, `quyen`, `lienhe`, `hinh`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'admin@gmail.com', '$2y$10$MbBBXv7uczxBi5uWKDQnsOR5KFLarmGYYH3E8etnFoEHhJbP.g1B6', 1, '01699150374', '1580551922.png', 'c0YCk0BgXPlxZ5KsvNXVpEaaNhmzQeeMAFOR6W1LaZNWQHUh0OqdQwhvMrMV', '2020-01-30 01:56:31', '2020-01-30 01:56:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `author`
--
ALTER TABLE `author`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bills`
--
ALTER TABLE `bills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bills_idcustomer_foreign` (`idCustomer`);

--
-- Indexes for table `bill_detail`
--
ALTER TABLE `bill_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `billdetail_idbill_foreign` (`idBill`),
  ADD KEY `billdetail_idbook_foreign` (`idBook`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`),
  ADD KEY `book_iduser_foreign` (`idUser`),
  ADD KEY `book_idloaisach_foreign` (`idLoaisach`),
  ADD KEY `book_idtacgia_foreign` (`idTacgia`),
  ADD KEY `book_idnhasanxuat_foreign` (`idNhasanxuat`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`),
  ADD KEY `feedback_iduser_foreign` (`idUser`),
  ADD KEY `feedback_idbook_foreign` (`idBook`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `publisher`
--
ALTER TABLE `publisher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_book`
--
ALTER TABLE `type_book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `author`
--
ALTER TABLE `author`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `bills`
--
ALTER TABLE `bills`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bill_detail`
--
ALTER TABLE `bill_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `publisher`
--
ALTER TABLE `publisher`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `slide`
--
ALTER TABLE `slide`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `type_book`
--
ALTER TABLE `type_book`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bills`
--
ALTER TABLE `bills`
  ADD CONSTRAINT `bills_idcustomer_foreign` FOREIGN KEY (`idCustomer`) REFERENCES `customer` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `bill_detail`
--
ALTER TABLE `bill_detail`
  ADD CONSTRAINT `billdetail_idbill_foreign` FOREIGN KEY (`idBill`) REFERENCES `bills` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `billdetail_idbook_foreign` FOREIGN KEY (`idBook`) REFERENCES `book` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `book`
--
ALTER TABLE `book`
  ADD CONSTRAINT `book_idloaisach_foreign` FOREIGN KEY (`idLoaisach`) REFERENCES `type_book` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_idnhasanxuat_foreign` FOREIGN KEY (`idNhasanxuat`) REFERENCES `publisher` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_idtacgia_foreign` FOREIGN KEY (`idTacgia`) REFERENCES `author` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `feedback_idbook_foreign` FOREIGN KEY (`idBook`) REFERENCES `book` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `feedback_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
