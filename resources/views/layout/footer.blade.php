<div id="footer" class="color-div">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="widget">
						<h4 class="widget-title">Facebook</h4>
						<div>
							<ul>
								<li><a href="https://www.facebook.com/nvchien1006"><i class="fa fa-chevron-right"></i> NV Chiến</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="widget">
						<h4 class="widget-title">Information</h4>
						<div>
							<ul>
								<li><a href="gioithieu"><i class="fa fa-chevron-right"></i> Giới thiệu</a></li>
								<li><a href="lienhe"><i class="fa fa-chevron-right"></i> Liên hệ</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
				 <div class="col-sm-10">
					<div class="widget">
						<h4 class="widget-title">Contact Us</h4>
						<div>
							<div class="contact-info">
								<i class="fa fa-map-marker"></i>
								<p>Trường đại học Bách Khoa Hà Nội</p>
								<p>Số 1, Đại Cồ Việt, Hai Bà Trưng, Hà Nội, Việt Nam</p>
							</div>
						</div>
					</div>
				  </div>
				</div>
			</div> <!-- .row -->
		</div> <!-- .container -->
	</div> <!-- #footer -->
	<div class="copyright">
		<div class="container">
			<p class="pull-left">Coppyright (&copy;) 2019</p>
			<p class="pull-right pay-options">
{{--				<a href="#"><img src="source/assets/dest/images/pay/master.jpg" alt="" /></a>--}}
			</p>
			<div class="clearfix"></div>
		</div> <!-- .container -->
	</div> <!-- .copyright -->
