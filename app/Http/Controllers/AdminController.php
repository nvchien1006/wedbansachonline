<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

    //
    public function getDangnhap(){
        return view('admin.auth.login');
    }

    public function postDangnhap(Request $request){
        $this->validate($request, [
            'tendangnhap' => 'required',
            'password' => 'required|min:3|max:6'
        ], [
            'tendangnhap.required' => 'Bạn chưa nhập tên đăng nhập',
            'password.required' => 'Bạn chưa nhập mật khẩu',
            'password.min' => 'Mật khẩu không nhở hơn 3 ký tự',
            'password.max' => 'Mật khẩu không lớn hơn 32 ký tự'
        ]);

        if(Auth::attempt(['tendangnhap'=>$request->tendangnhap,'password'=>$request->password])){
            return redirect()->route('danhsachTypebook');
        }else{
            return redirect()->back()->with('thongbao','Tài khoản hoặc mật khẩu không đúng');
        }

    }

    public function dangxuat()
    {
        Auth::logout();
        return view('admin.auth.login');

    }
}
