@extends('layout.index')
@section('content')
    <div class="inner-header">
        <div class="container">
            <div class="pull-left">
                <h6 class="inner-title">Sản phẩm {{$book->ten}}</h6>
            </div>

            <div class="pull-right">
                <div class="beta-breadcrumb font-large">
                    <a href="trangchu ">Home</a> / <span>Thông tin sản phẩm</span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    	@if (session('thongbao'))
    				<div class="alert alert-danger">
    				  	{{session('thongbao')}}
    				</div>
    	@endif

    <div class="container">
        <div id="content">
            <div class="row">
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-4">
                            <img src="source/upload/book/{{$book->hinh}}" alt="" width="300px" height="350px">
                        </div>
                        <div class="col-sm-8">
                            <div class="single-item-body">
                                <p class="single-item-title">
                                <h2>{{$book->ten}}</h2><br>
                                <p class="single-item-price">
									<span>Tác giả :
									 {{$book->author->ten}}</span>
                                </p><br>
                                <p class="single-item-price">
									<span>Giá tiền :
										{{number_format($book->giatien)}} VNĐ</span>
                                </p><br>
                                <p class="single-item-price">
									<span>Phí giao hàng:
										{{number_format($book->phigiaohang)}} VNĐ</span>
                                </p><br>
                                <p class="single-item-price">
									<span>Số lượng :
									{{$book->soluottruycap}}</span>
                                </p>
                            </div>

                            <div class="clearfix"></div>
                            <div class="space20">&nbsp;</div>

                            <!-- <p>Số lượng</p> -->

                            <div class="single-item-options">
                                <select class="wc-select" name="soluong" id="soluong" onchange="change()">
                                    @for ($i=1; $i<=15; $i++)
                                        <option value="{{$i}}">{{$i}}</option>
                                    @endfor
                                </select>
                                <p id="link_cart">
                                    <a class="add-to-cart" href="themgiohang/{{$book->id}}/{{1}}"><i
                                            class="fa fa-shopping-cart"></i></a>
                                </p>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                    <div class="space40">&nbsp;</div>
                    <div class="woocommerce-tabs">
                        <ul class="tabs">
                            <li><a href="#tab-description">Mô tả</a></li>
                        </ul>

                        <div class="panel" id="tab-description">
                            <p>{{$book->mota}}</p>
                        </div>
                    </div>
                    <!-- Comments Form -->
                    @if (Auth::user())
                        <div class="well">
                            <h4>Viết bình luận ...</h4>
                            <form action="feedback/{{$book->id}}" method="Post" role="form">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="form-group">
                                    <textarea class="form-control" name="noidung" rows="3"></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary">Gửi</button>
                            </form>
                        </div>

                        <hr>
                    @endif
                    <h4>Bình Luận : </h4>
                    <!-- Posted Comments -->

                    <!-- Comment -->
                    @foreach ($book->feedback as $key)
                        <div class="media">
                            <a class="pull-left">
                                <img class="media-object" src="source/upload/user/{{$key->user->hinh}}" alt=""
                                     width="40px" height="50px">
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading"
                                    style="font-size: 18px; font-weight: bold;">  {{$key->user->name}}
                                    <small>  {{$key->created_at}}</small>
                                </h4>
                                <p style="font-size: 16px">{{$key->noidung}}</p>
                            </div>
                        </div>
                    @endforeach
                    <div class="space50">&nbsp;</div>
                    <div class="beta-products-list">
                        <h4>Sản phẩm tương tự</h4>

                        <div class="row">
                            @foreach ($booksame as $key)
                                <div class="col-sm-4">
                                    <div class="space20">&nbsp;</div>
                                    <div class="single-item">
                                        <div class="single-item-header">
                                            <a href="chitietsanpham/{{$key->id}}"><img
                                                    src="source/upload/book/{{$key->hinh}}" alt="" width="100px"
                                                    height="150px"></a>
                                        </div>
                                        <div class="single-item-body">
                                            <p class="single-item-title">{{$key->ten}}</p>
                                            <p class="single-item-price">
                                                <span>{{number_format($key->giatien)}} VNĐ</span>
                                            </p>
                                        </div>
                                        <div class="space10">&nbsp;</div>
                                        <div class="single-item-caption">
                                            <a class="add-to-cart pull-left" href="chitietsanpham/{{$key->id}}"><i
                                                    class="fa fa-shopping-cart"></i></a>
                                            <a class="beta-btn primary" href="chitietsanpham/{{$key->id}}">Chi tiết <i
                                                    class="fa fa-chevron-right"></i></a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        {{--						<div class="row"> {{$booksame->links()}}</div>--}}
                    </div> <!-- .beta-products-list -->
                </div>

                <div class="col-sm-3 aside">
                    <div class="widget">
                        <h3 class="widget-title">Sản phẩm mới nhất</h3>
                        <div class="widget-body">
                            @foreach ($newbook as $key)
                                <div class="beta-sales beta-lists">
                                    <div class="media beta-sales-item">
                                        <a class="pull-left" href="chitietsanpham/{{$key->id}}">
                                            <img src="source/upload/book/{{$key->hinh}}" alt="" width="55px"
                                                 height="56px">
                                        </a>
                                        <div class="media-body">
                                            <a href="chitietsanpham/{{$key->id}}">{{$key->ten}}</a><br>
                                            <span class="beta-sales-price">{{number_format($key->giatien)}} VNĐ</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div> <!-- best sellers widget -->
                </div>
            </div>
        </div> <!-- #content -->
    </div> <!-- .container -->
@endsection
@section('script')
    	<script language="javascript">
    		function change(){
    			var s = document.getElementById("soluong").value;
    			document.getElementById("link_cart").innerHTML = "<a class='add-to-cart' href='themgiohang/{{$book->id}}/"+s+"'><i class='fa fa-shopping-cart'></i></a>";
    		};
    	</script>
@endsection
