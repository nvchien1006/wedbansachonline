<?php

namespace App\Http\Controllers;

use App\Author;
use App\Bill;
use App\Book;
use App\Cart;
use App\Customer;
use App\DetailBill;
use App\Feedback;
use App\Publisher;
use App\Slide;
use App\TypeBook;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class PageController extends Controller
{
    /**
     * PageController constructor.
     */

    public $userImgFolder = 'source/upload/user/';
    public $booksImgFolder = 'source/upload/book/';

    public function __construct()
    {
        $typebook = TypeBook::all();
        $author = Author::all();
        $publisher = Publisher::all();
        $user1 = Auth::user();

        view()->share('author', $author);
        view()->share('publisher', $publisher);
        view()->share('typebook', $typebook);
        view()->share('user', $user1);
    }

    public function trangChu()
    {
        $slide = Slide::all();
        $newBooks = Book::where('trangthai', 1)->paginate(4,['*'],'new');
        $allBooks = Book::paginate(4,['*'],'all');

        return view('pages.trangchu', ['slide' => $slide, 'newbook' => $newBooks, 'allbook' => $allBooks]);

    }

    public function gioiThieu()
    {
        return view('pages.gioithieu');
    }

    public function lienHe()
    {
        return view('pages.lienhe');
    }

    public function timKiem(Request $request)
    {
        $key = $request->timkiem;
        $timkiems = Book::where('ten', 'like', "%$key%")->orWhere('mota', 'like', "%$key%")->paginate(12);
        return view('pages.timkiem', ['timkiems' => $timkiems, 'key' => $key]);
    }

    public function loaiSanPham($id)
    {
        $booktype = Book::where('idLoaisach', $id)->paginate(12);
        return view('pages.loaisanpham', ['booktype' => $booktype]);
    }

    public function tacgia($id)
    {
        $bookauthor = Book::where('idTacgia', $id)->paginate(12);
        return view('pages.tacgia', ['bookauthor' => $bookauthor]);
    }

    public function nhasanxuat($id)
    {
        $bookpublisher = Book::where('idNhasanxuat', $id)->paginate(12);
        return view('pages.nhasanxuat', ['bookpublisher' => $bookpublisher]);
    }

    public function chitietsp($id)
    {
        $book = Book::where('id', $id)->first();
        $booksame = Book::where('idLoaisach', $book->idLoaisach)->paginate(3);
        $newbook = Book::where('trangthai', 1)->take(5)->get();

        return view('pages.chitietsanpham',
            ['book' => $book, 'booksame' => $booksame, 'newbook' => $newbook]);
    }

    //Login-Signin-SignUp
    public function getDangnhap()
    {
        return view('pages.dangnhap');
    }

    public function postDangnhap(Request $request)
    {
        $this->validate($request,
            ['tendangnhap' => 'required',
                'password' => 'required'],

            ['tendangnhap.required' => 'Nhập tên đăng nhập',
                'passowrd.required' => 'Nhập mật khẩu']
        );

        if (Auth::attempt(['tendangnhap' => $request->tendangnhap, 'password' => $request->password])) {
            return redirect('trangchu');
        } else {
            return redirect('dangnhap')->with('thongbao', 'Đăng nhập không thành công!');
        }

    }

    public function dangXuat()
    {
        Auth::logout();
        return redirect('trangchu');
    }

    public function getDangKy()
    {
        return view('pages.dangky');
    }

    public function postDangKy(Request $request)
    {
        //
        $this->validate($request,
            ['ten' => 'required|min:3',
                'email' => 'required| unique:users,email',
                'tendangnhap' => 'required| unique:users,tendangnhap',
                'password' => 'required|min:6',
                'repassword' => 'required|same:password',
                'lienhe' => 'required',
            ],
            [
                'ten.min' => 'Tên lớn hơn hoặc bằng 3 ký tự',
                'ten.required' => 'Chưa nhập tên người dùng',
                'email.required' => 'Chưa nhập Email',
                'email.unique' => 'Email đã được sử dụng',
                'tendangnhap.required' => 'Chưa nhập tên đăng nhập',
                'tendangnhap.unique' => 'Tên đăng nhập đã được sử dụng',
                'password.required' => 'Chưa nhập mật khẩu',
                'password.min' => 'Mật khẩu lớn hơn 6 ký tự',
                'repassword.required' => 'Chưa nhập nhập lại mật khẩu',
                'repassword.same' => 'Nhập lại mật khẩu không đúng',
                'lienhe.required' => 'Chưa nhập liên hệ',
            ]
        );

        //
        $user = new User();
        $user->name = $request->ten;
        $user->tendangnhap = $request->tendangnhap;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->lienhe = $request->lienhe;
        $user->quyen = 0;
        $user->hinh = 0;
        $user->save();

        return redirect('dangky')->with('thongbao', 'Đăng ký thành công!');
    }

    public function getThongtincanhan()
    {
        return view('pages.chinhsuathongtincanhan')->with('user', Auth::user());
    }

    public function postThongtincanhan(Request $request)
    {
        $this->validate($request, [
            'ten' => 'required|min:3',
            'password' => 'required|min:6|max:32',
            'passwordAgain' => 'required|same:password',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg'

        ],
            [
                'ten.min' => 'Tên lớn hơn hoặc bằng 3 ký tự',
                'ten.required' => 'Chưa nhập tên người dùng',
                'password.required' => 'Bạn chưa nhập mật khẩu',
                'password.min' => 'Mật khẩu có ít nhất 6 kí tự đến 32 kí tự',
                'password.max' => 'Mật khẩu có ít nhất 6 kí tự đến 32 kí tự',
                'passwordAgain.required' => 'Bạn chưa nhập lại mật khẩu',
                'passwordAgain.same' => 'Mật khẩu không trùng khớp',
                'image.mimes' => 'Định dạng file không hỗ trợ'
            ]);

        $user = Auth::user();
        $user->name = $request->ten;
        $user->password = bcrypt($request->password);

        //load file
        if ($request->hasFile('hinh')) {
            $file = $request->file('hinh');
            $newName = time() . '____.' . $file->getClientOriginalExtension();

            $file->move($this->userImgFolder, $newName);
            $user->hinh = $newName;
        }

        $user->save();

        return redirect('suathongtincanhan')
            ->with('user', Auth::user())
            ->with('thongbao', 'Sửa thông tin thành công');
    }

    public function postFeedback(Request $request, $bookId)
    {
        $feedback = new Feedback();
        $feedback->idBook = $bookId;
        $feedback->idUser = Auth::user()->id;
        $feedback->noidung = $request->noidung;
        $feedback->save();
        return \redirect()->back();
    }

    public function postThemBook(Request $request)
    {
        $this->validate($request,
            [
                'ten' => 'required',
                'nhasanxuat' => 'required',
                'mota' => 'required',
                'tacgia' => 'required',
                'sotrang' => 'required|integer',
                'giatien' => 'required|integer',
                'phigiaohang' => 'required|integer',
            ],
            [
                'ten.required' => 'Bạn chưa nhập tên sách',
                'nhasanxuat.required' => 'Bạn chưa nhập Nhà suất bản',
                'mota.required' => 'Bạn chưa nhập mô tả',
                'tacgia.required' => 'Bạn chưa nhập Tác giả',

                'sotrang.required' => 'Bạn chưa nhập số trang',
                'sotrang.integer' => 'Số trang phải là chữ số',

                'giatien.required' => 'Bạn chưa nhập giá tiền',
                'giatien.integer' => 'Số trang phải là chữ số',

                'phigiaohang.required' => 'Bạn chưa nhập phí giao hàng',
                'phigiaohang.integer' => 'Số trang phải là chữ số'

            ]);


        $author = new Author();
        $author->ten = $request->tacgia;
        $author->tenkhongdau = $request->tacgia;
        $author->save();

        $publisher = new Publisher();
        $publisher->ten = $request->nhasanxuat;
        $publisher->tenkhongdau = $request->nhasanxuat;
        $publisher->save();

        $newbooks = new Book();
        $newbooks->ten = $request->ten;
        $newbooks->mota = $request->mota;
        $newbooks->sotrang = $request->sotrang;
        $newbooks->giatien = $request->giatien;
        $newbooks->soluottruycap = $request->soluottruycap;
        $newbooks->trangthai = $request->trangthai;
        $newbooks->phigiaohang = $request->phigiaohang;
        $newbooks->idUser = Auth::user()->id;
        $newbooks->idLoaisach = $request->idLoaisach;
        $newbooks->idTacgia = $author->id;
        $newbooks->idNhasanxuat = $publisher->id;

        if ($request->hasFile('hinh')) {
            $file = $request->file('hinh');
            $newImgName = time() . '__.' . $file->getClientOriginalExtension();
            $file->move($this->booksImgFolder, $newImgName);
            $newbooks->hinh = $newImgName;
        } else {
            $newbooks->hinh = "";
        }

        $newbooks->save();

        //
        return \redirect()->back()->with('thongbao', 'Thêm sách thành công');
    }

    public function getThemBook(Request $request)
    {
        return view('pages.upload');

    }

    public function themGioHang($id, $num)
    {
        $book = Book::find($id);
        if ($book->soluottruycap >= $num) {
            $oldCart = Session('cart') ? Session::get('cart') : null;
            $newCart = new Cart($oldCart);
            $newCart->add($book, $id, $num);
            Session::put('cart', $newCart);
            return redirect()->back();
        } else {
            return redirect()->back()->with('thongbao', 'Quá số lượng còn lại');
        }
    }


    public function xoaGioHang($id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);
        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }
        return \redirect()->back();
    }

    public function getDatHang(){
        return view('pages.dathang');
    }

    public function postDatHang(Request $request){

        $cart = Session::get('cart');

        //save customer
        $customer = new Customer();
        $customer->ten = $request->ten;
        $customer->gioitinh =$request->gioitinh;
        $customer->email =$request->email;
        $customer->diachi =$request->diachi;
        $customer->sodienthoai =$request->sodienthoai;
        $customer->notes =$request->notes;
        $customer->save();

        //save bill
        $bill = new Bill();
        $bill ->idCustomer= $customer->id;
        $bill->ngaydonhang =date('Y-m-d');
        $bill->tongtien =$cart->totalPrice+$cart->getAllShipFee();
        $bill->trangthai=0;
        $bill->notes = $request->notes;
        $bill->save();

        //save detailbill
        foreach ($cart->items as $key => $value) {
            $detailBill = new DetailBill();
            $detailBill->idBill=$bill->id;
            $detailBill->idBook=$key;
            $detailBill->soluong=$value['qty'];
            $detailBill->giatien=$value['toltalPrice']/$value['qty'];
            $detailBill->save();
        }
//
        Session::forget('cart');
        return \redirect()->back()->with('thongbao', 'Đặt hàng thành công');
    }

}
