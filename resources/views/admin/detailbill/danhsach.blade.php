@extends('admin.layout.index')

@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Hóa đơn chi tiết / Bills detail
                            <small>Danh sách </small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if (session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>ID Bill</th>
                                <th>ID sách</th>
                                <th>Số lượng</th>
                                <th>Ngày hóa đơn</th>                         
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($detailbill as $key)
                            <tr class="odd gradeX" align="center">
                                <td>{{$key->id}}</td>
                                <td>{{$key->idBill}}</td>
                                <td>{{$key->idBook}}</td>
                                <td>{{$key->soluong}}</td> 
                                <td>{{$key->created_at}}</td>                                
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/detailbill/xoa/{{$key->id}}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/detailbill/sua/{{$key->id}}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection