@extends('layout.index')
@section('content')
    <div class="inner-header">
        <div class="container">
            <div class="pull-left">
                <h6 class="inner-title"><a href="upload/danhsach">Danh sách upload</h6>
            </div>
            <div class="pull-right">
                <div class="beta-breadcrumb">
                    <a href="trangchu">Home</a> / <span>Upload book</span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="container">
        <div id="content">
            @if (count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        {{$err}}<br>
                    @endforeach
                </div>
            @endif
            @if (session('thongbao'))
                <div class="alert alert-success">
                    {{session('thongbao')}}
                </div>
            @endif
            <form action="upload/them" method="post" class="beta-form-checkout" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <h4>Upload</h4>
                        <div class="space20">&nbsp;</div>
                        <div class="form-block">
                            <label for="ten">Tên sách *:</label>
                            <input type="text" id="ten" name="ten" required>
                        </div>
                        <div class="form-block">
                            <label for="your_last_name">Thể loại *:</label>
                            <select class="form-control" name="idLoaisach">
                                @foreach ($typebook as $key)
                                    <option value="{{$key->id}}">{{$key->ten}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-block">
                            <label for="mota">Mô tả *:</label>
                            <textarea name="mota" cols="2"></textarea>
                        </div>
                        <div class="form-block">
                            <label for="your_last_name">Hình ảnh *:</label>
                            <input type="file" id="hinh" name="hinh" required>
                        </div>
                        <div class="form-block">
                            <label for="your_last_name">Nhà sản xuất *:</label>
                            <input type="text" id="nhasanxuat" name="nhasanxuat" required>
                        </div>
                        <div class="form-block">
                            <label for="your_last_name">Tác giả *:</label>
                            <input type="text" id="tacgia" name="tacgia" required>
                        </div>
                        <div class="form-block">
                            <label for="your_last_name">Số trang *:</label>
                            <input type="text" id="sotrang" name="sotrang" required>
                        </div>
                        <div class="form-block">
                            <label for="your_last_name">Giá tiền *:</label>
                            <input type="text" id="giatien" name="giatien" required>
                        </div>
                        <div class="form-group">
                            <label for="trangthai">Trạng thái *: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <label class="radio-inline">
                                <input name="trangthai" value="1" checked="" type="radio">Mới
                            </label>
                            <label class="radio-inline">
                                <input name="trangthai" value="0" type="radio">Cũ
                            </label>
                        </div>
                        <div class="form-block">
                            <label for="phone">Số lượng *:</label>
                            <input type="text" id="soluottruycap" name="soluottruycap" required>
                        </div>
                        <div class="form-block">
                            <label for="phone">Phí giao hàng *:</label>
                            <input type="text" id="phigiaohang" name="phigiaohang" required>
                        </div>
                        <button type="submit" class="btn btn-default btn-md">Thêm</button>
                        <button type="reset" class="btn btn-default">Làm mới</button>
                    </div>
                    <div class="col-sm-2"></div>
                </div>
            </form>
        </div> <!-- #content -->
    </div> <!-- .container -->
@endsection
