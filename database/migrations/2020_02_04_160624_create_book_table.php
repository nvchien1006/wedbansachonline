<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('ten');
            $table->string('tenkhongdau');
            $table->text('mota');
            $table->integer('sotrang');
            $table->integer('giatien');
            $table->string('hinh');
            $table->integer('soluottruycap');
            $table->tinyInteger('trangthai');
            $table->unsignedBigInteger('idUser');
            $table->unsignedBigInteger('idLoaisach');
            $table->unsignedBigInteger('idTacgia');
            $table->unsignedBigInteger('idNhasanxuat');
            $table->integer('phigiaohang');

            $table->foreign('idUser')->references('id')->on('users')->onDelete('cascade');;
            $table->foreign('idLoaisach')->references('id')->on('type_book')->onDelete('cascade');;
            $table->foreign('idTacgia')->references('id')->on('author')->onDelete('cascade');;
            $table->foreign('idNhasanxuat')->references('id')->on('publisher')->onDelete('cascade');;

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book');
    }
}
