<?php

namespace App;

class Cart
{
	public $items = null;
	public $totalQty = 0;
	public $totalPrice = 0;
//    public $totalFeeShip = 0;

	public function __construct($oldCart){
		if($oldCart){
			$this->items = $oldCart->items;
			$this->totalQty = $oldCart->totalQty;
			$this->totalPrice = $oldCart->totalPrice;
//			$this->totalFeeShip = $oldCart->totalFeeShip;
		}
	}

	public function getAllShipFee(){
        $shipFee = 0;
	    if($this->items){
            foreach ($this->items as $item){
                $shipFee += $item['item']['phigiaohang'];
            }
        }
        return $shipFee;
	}

	//neu not add >>error
	public function add($book, $idBook, $addNum){
		$item = ['qty'=>0, 'toltalPrice' => 0, 'item' => $book];
		if($this->items){
			if(array_key_exists($idBook, $this->items)){
				$item = $this->items[$idBook];
			}
		}

        $addPrice = $addNum * $book->giatien;
		$item['qty']+=$addNum;
		$item['toltalPrice'] += $addPrice;

		$this->items[$idBook] = $item;
		$this->totalQty+=$addNum;
		$this->totalPrice += $addPrice;
	}

	public function removeItem($id){


		$this->totalQty-=$this->items[$id]['qty'];
		$this->totalPrice -= $this->items[$id]['toltalPrice'];

		unset($this->items[$id]);

	}
}
