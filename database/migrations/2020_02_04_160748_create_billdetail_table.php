<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBilldetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_detail', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('idBill');
            $table->unsignedBigInteger('idBook');
            $table->integer('soluong');
            $table->float('giatien');

            $table->foreign('idBill')->references('id')->on('bills')->onDelete('cascade');;
            $table->foreign('idBook')->references('id')->on('book')->onDelete('cascade');;

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billdetail');
    }
}
