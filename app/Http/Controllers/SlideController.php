<?php

namespace App\Http\Controllers;

use App\Slide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class SlideController extends Controller
{
    //
    public function listSlide(){
        $slide=Slide::all();
        return view('admin.slide.danhsach', ['slide'=>$slide]);
    }

    public function getAddSlide(){
        return view('admin.slide.them');
    }
    public function postAddSlide(Request $request){
        $this->validate($request,
            [
                'ten'=>'required',
            ],
            [
                'ten.required'=>'Bạn chưa nhập tên',
            ]);

        $slide=new Slide;
        $slide->ten=$request->ten;
        if($request->has('link')) {
            $slide->link =$request->link;
        }else{
            $slide->link = "";
        }

        if ($request->hasFile('hinh')) {
            $file = $request->file('hinh');
            $newFileName = time() . '.' . $file->getClientOriginalExtension();
            $file->move(Config::get('MyConst.slide_img_folder'), $newFileName);
            $slide->hinh = $newFileName;
        }else{
            $slide->hinh = "";

        }

        $slide->save();

        return redirect('admin/slide/them')->with('thongbao', 'Thêm thành công');
    }

    public function getEditSlide($id){
        $slide=Slide::find($id);
        return view('admin.slide.sua', ['slide'=>$slide]);
    }

    public function postEditSlide(Request $request, $id){
        $this->validate($request,
            [
                'ten'=>'required',
            ],
            [
                'ten.required'=>'Bạn chưa nhập tên',
            ]);

        $slide=Slide::find($id);

        $slide->ten=$request->ten;

        if($request->has('link')) {
            $slide->link =$request->link;
        }

        if ($request->hasFile('hinh')) {
            $file = $request->file('hinh');
            $newFileName = time() . '.' . $file->getClientOriginalExtension();
            $file->move(Config::get('MyConst.slide_img_folder'), $newFileName);
            $slide->hinh = $newFileName;
        }

        $slide->save();

        return redirect('admin/slide/sua/'.$id)->with('thongbao', 'Sửa thành công');
    }

    public function xoaSlide($id){
        $slide=Slide::find($id);
        if ($slide != null){
            $slide->delete();
            return redirect()->back()->with('thongbao', 'Xóa thành công');
        }
    }

}
