<?php
return [
    'author_img_folder'=>'source/upload/author',
    'publisher_img_folder'=>'source/upload/publisher',
    'book_img_folder'=>'source/upload/book',
    'slide_img_folder'=>'source/upload/slide',
    'user_img_folder'=>'source/upload/user'
];
