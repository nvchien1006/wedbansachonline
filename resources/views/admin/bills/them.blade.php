@extends('admin.layout.index')

@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Hóa đơn / Bills
                            <small>Thêm</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if (count($errors)>0)    
                            <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                            </div>
                        @endif
                        @if (session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                        <form action="admin/bills/them" method="POST">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label>Khách hàng</label>
                                <select class="form-control" name="idCustomer">
                                    @foreach ($customer as $key)
                                    <option value="{{$key->id}}">{{$key->ten}}</option>                                    
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Ngày đơn hàng</label>
                                <input type="date" class="form-control" name="ngaydonhang" />
                            </div>                          
                            <div class="form-group">
                                <label>Tổng tiền</label>
                                <input class="form-control" name="tongtien" placeholder="Nhập tên tổng tiền" />
                            </div>
                            <div class="form-group">
                                <label>Chú ý</label>
                                <textarea name="notes" id="notes" class="form-control ckeditor" rows="4"></textarea>
                            </div> 
                            <div class="form-group">
                                <label>Trạng thái</label>
                                <label class="radio-inline">
                                    <input name="trangthai" value="0" checked="" type="radio">Chưa thanh toán
                                </label>
                                <label class="radio-inline">
                                    <input name="trangthai" value="1" type="radio">Đã hoàn thành
                                </label>
                            </div>                            
                            <button type="submit" class="btn btn-default">Thêm</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection